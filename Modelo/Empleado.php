<?php

require_once('Conexion.php');
require_once('Persona.php');

class Empleado extends Persona {
	
	private $idEmpleado;
	private $usuario;
	private $contrasena;
	private $estado;
	
	
	public function __construct($empleado_data=array()){
		
        parent::__construct();
		if(count($empleado_data)>1){
			foreach ($empleado_data as $campo => $valor){
               $this->$campo = $valor;
			}
		}else {
			$this->idEmpleado="";
			$this->usuario="";
			$this->contrasena="";
			$this->estado="";
			
		}
	}
	
	
	public function setIdEmpleado ($idEmpleado){
		$this->idEmpleado = $idEmpleado;
	}
	public function getIdEmpleado (){
		return $this->idEmpleado;
	}
	
	public function setIdentificacion ($identificacion){
		$this->identificacion = $identificacion;
	}
	
	public function getIdentificacion (){
		return $this->identificacion;
	}
	
	public function setNombres ($nombres){
		$this->nombres = $nombres;
	}
	
	public function getNombres (){
		return $this->nombres;
	}
	public function setApellidos ($apellidos){
		$this->apellidos = $apellidos;
	}
	
	public function getApellidos (){
		return $this->apellidos;
	}
	public function setDireccion ($direccion){
		$this->direccion = $direccion;
	}
	
	public function getDireccion (){
		return $this->direccion;
	}
	public function setTelefono ($telefono){
		$this->telefono = $telefono;
	}
	
	public function getTelefono (){
		return $this->telefono;
	}
	public function setEmail ($email){
		$this->cedula = $cedula;
	}
	
	public function getEmail (){
		return $this->email;
	}
	
	public function setTipoUsuario ($tipoUsuario){
		$this->tipoUsuario = $tipoUsuario;
	}
	
	public function getTipoUsuario (){
		return $this->tipoUsuario;
	}
	
	public function setUsuario ($usuario){
		$this->usuario = $usuario;
	}
	
	public function getUsuario (){
		return $this->usuario;
	}
	
	public function setContrasena($contrasena){
		$this->contrasena = $contrasena;
	}
	public function getContrasena(){
		return $this->contrasena;
	}
	public function setEstado ($estado){
		$this->estado = $estado;
	}
	
	public function getEstado (){
		return $this->estado;
	}
	
	public static function buscarForId($id){
		if ($id > 0){
			$emp = new Empleado();
			$getrow = $emp->getRow("SELECT * FROM persona WHERE idPersona =?", array($id));
			$emp->idEmpleado = $getrow['idPersona'];
			$emp->identificacion = $getrow['identificacion'];
			$emp->nombres = $getrow['nombres'];
			$emp->apellidos = $getrow['apellidos'];
			$emp->direccion = $getrow['direccion'];
			$emp->telefono = $getrow['telefono'];
			$emp->email = $getrow['email'];
			$emp->tipoUsuario = $getrow['tipoUsuario'];
			$emp->usuario = $getrow['usuario'];
			$emp->contrasena = $getrow['contrasena'];
			$emp->estado = $getrow['estado'];
			
			return $emp;
		}else{
			return NULL;
		}
		$this->Disconnect();
	}
	public static function buscarAll(){
		return Persona::buscar("idPersona");	
	}
	
	public static function buscar($campo, $valor=array()){
        $arrEmpleado = array();
		$tmp = new Empleado();
		if (count($valor) > 0){
        	$getrows = $tmp->getRows("SELECT * FROM persona WHERE ".$campo." = ?", array($valor));
		}else{
        	$getrows = $tmp->getRows("SELECT * FROM persona", $valor);
		}
        foreach ($getrows as $getrow) {
            $emp = new Empleado();
			
			$emp->idEmpleado = $getrow['idPersona'];
			$emp->identificacion = $getrow['identificacion'];
			$emp->nombres = $getrow['nombres'];
			$emp->apellidos = $getrow['apellidos'];
			$emp->direccion = $getrow['direccion'];
			$emp->telefono = $getrow['telefono'];
			$emp->email = $getrow['email'];
			$emp->tipoUsuario = $getrow['tipoUsuario'];
			$emp->usuario = $getrow['usuario'];
			$emp->contrasena = $getrow['contrasena'];
			$emp->estado = $getrow['estado'];
			
            array_push($arrEmpleado, $emp);
			$emp->Disconnect();
        }
        
        return $arrEmpleado;
    }
	
	public static function buscarForString($campo, $valor=array()){
        $arrEmpleado = array();
		$tmp = new Empleado();
		if (count($valor) > 0){
			$tipo = 'tipoUsuario';
        	$getrows = $tmp->getRows("SELECT * FROM persona WHERE ".$tipo." = ? AND ".$campo." = ?", array('Vendedor', $valor));
		}
        foreach ($getrows as $getrow) {
            $emp = new Empleado();
			$emp->idEmpleado = $getrow['idPersona'];
			$emp->identificacion = $getrow['identificacion'];
			$emp->nombres = $getrow['nombres'];
			$emp->apellidos = $getrow['apellidos'];
			$emp->direccion = $getrow['direccion'];
			$emp->telefono = $getrow['telefono'];
			$emp->email = $getrow['email'];
			$emp->tipoUsuario = $getrow['tipoUsuario'];
			$emp->usuario = $getrow['usuario'];
			$emp->contrasena = $getrow['contrasena'];
			$emp->estado = $getrow['estado'];
            array_push($arrEmpleado, $emp);
			$emp->Disconnect();
        }
        
        return $arrEmpleado;
    }
	
    public function inactivar($id){
	
	}
	
	public function registrar (){
	
	try {
			$this->insertRow(" INSERT INTO persona (idPersona, tipoUsuario, identificacion, nombres, apellidos, direccion, telefono,email, usuario, contrasena,estado) VALUES ('NULL',?, ?, ?, ?, ?, ?, ?, ?,?,?)",         array( 
				    $this->tipoUsuario,
					$this->identificacion,
					$this->nombres,
					$this->apellidos,
					$this->direccion,
					$this->telefono,
					$this->email,
					$this->usuario,
			        $this->contrasena,
					$this->estado
					)
			);
			$this->Disconnect();
			return true;
		}catch(Exception $e) {
		throw new Exception($e->getMessage());
			return false;
		}
	}
	
	public function actualizar (){
		
		try {
			$this->updateRow("UPDATE persona SET identificacion = ?, nombres = ?, apellidos = ?, direccion = ?, telefono = ?, email = ?, tipoUsuario = ?, usuario = ?, contrasena = ?, estado = ? WHERE idPersona = ?", array( 
			        $this->identificacion,
					$this->nombres,
					$this->apellidos,
					$this->direccion,
					$this->telefono,
					$this->email,
				    $this->tipoUsuario,
					$this->usuario,
			        $this->contrasena,
					$this->estado,
					
				    $this->idEmpleado
				)
			);
			$this->Disconnect();
			return true;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
			return false;
		}
		
	}
	
	public  function login($usuario , $contrasena){
            
		$temp = new Empleado();	
        $getrows = $temp->getRows("SELECT * FROM persona WHERE usuario = ? AND contrasena = ?", array($usuario, $contrasena));
		
        if (count($getrows) > 0){
            if($getrows[0]['contrasena'] == $contrasena){
				foreach ($getrows as $getrow) {
					$per = new Empleado();
					$per->idPersona = $getrow['idPersona'];				
					$per->tipoUsuario = $getrow['tipoUsuario'];
					$per->identificacion = $getrow['identificacion'];
					$per->nombres = $getrow['nombres'];
					$per->apellidos = $getrow['apellidos'];
					$per->direccion = $getrow['direccion'];
					$per->telefono = $getrow['telefono'];
					$per->email = $getrow['email'];
					$per->usuario = $getrow['usuario'];
					$per->contrasena = $getrow['contrasena'];
					$per->estado = $getrow['estado'];
					$per->Disconnect();
					return $per;
				}                
            }else{
                return 2;
            }
        }else{
            return 3;
        }
    }
	
	
}
?>


			
		 
		
		
  