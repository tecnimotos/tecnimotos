<?php
require_once('Conexion.php');
require_once('Venta.php');
require_once('Repuesto.php');

class Novedad extends Conexion {
	
	private $idNovedad;
	private $tipoNovedad;
	private $fechaNovedad;
	private $descripcion;
	private $excedente;
	private $reembolso;
	private $repuestoEntrante;
	private $repuestoSaliente;
	private $venta;
    
		
	public function __construct($Novedad_data=array()){
        parent::__construct();
		if(count($Novedad_data)>1){
			foreach ($Novedad_data as $campo => $valor){
               $this->$campo = $valor;
			}
		}else {
			$this->idNovedad = "";
			$this->tipoNovedad = "";
			$this->fechaNovedad="";
			$this->descripcion="";
			$this->excedente = "";
			$this->reembolso = "";
			$this->repuestoEntrante = new Repuesto;
			$this->repuestoSaliente= new Repuesto;
			$this->venta = new Venta;
			
			      

		}
    }
	
	function __destruct (){ }
	
	public function setIdNovedad ($idNovedad){
		$this->idNovedad = $idNovedad;
	}
	
	public function getIdNovedad (){
		return $this->idNovedad;
	}	
	
	public function setRepuestoEntrante ($repuestoEntrante){
		$this->repuestoEntrante = $repuestoEntrante;
	}
	
	public function getRepuestoEntrante (){
		return $this->repuestoEntrante;
	}
	
		
	public function setRepuestoSaliente ($repuestoSaliente){
		$this->repuestoSaliente = $repuestoSaliente;
	}
	
	public function getRepuestoSaliente (){
		return $this->repuestoSaliente;
	}
	
	public function setFechaNovedad ($fechaNovedad){
		$this->fechaNovedad = $fechaNovedad;
	}
	
	public function getFechaNovedad (){
		return $this->fechaNovedad;
	}
	
	public function setDescripcion ($descripcion){
		$this->descripcion = $descripcion;
	}
	
	public function getDescripcion (){
		return $this->descripcion;
	}
		public function setExcedente ($excedente){
		$this->excedente = $excedente;
	}
	
	public function getExcedente (){
		return $this->excedente;
	}
	
	public function setReembolso ($reembolso){
		$this->reembolso = $reembolso;
	}
	
	public function getReembolso (){
		return $this->reembolso;
	}
	public function setTipoNovedad ($tipoNovedad){
		$this->tipoNovedad = $tipoNovedad;
	}
	
	public function getTipoNovedad (){
		return $this->tipoNovedad;
	}
	public function setVenta ($venta){
		$this->venta = $venta;
	}
	
	public function getVenta (){
		return $this->venta;
	}
	
	
	
    public static function buscarForId($id){
		if ($id > 0){
			$ins = new Novedad();
			$getrow = $ins->getRow("SELECT * FROM Novedad WHERE idNovedad =?", array($id));
			$ins->idNovedad = $getrow['idNovedad'];
			$ins->tipoNovedad = $getrow['tipoNovedad'];
			$ins->fechaNovedad = $getrow['fechaNovedad'];
			$ins->descripcion = $getrow['descripcion'];
			$ins->excedente = $getrow['excedente'];
			$ins->reembolso = $getrow['reembolso'];
			
			$ins->repuestoEntrante = Repuesto::buscarForId($getrow['repuestoEntrante']);
			$ins->repuestoSaliente = Repuesto::buscarForId($getrow['repuestoSaliente']);
			$ins->venta = Venta::buscarForId($getrow['venta']);
			
			

			$ins->Disconnect();
			return $ins;
		}else{
			return NULL;
		}
		$this->Disconnect();
	}
	
	public static function buscarAll(){
		return Novedad::buscar("idNovedad");
	}
	public static function buscarCambio(){
        return Novedad::buscar("select * from novedad where tipoNovedad In('Cambio')");
		
		}
	public static function buscar($campo, $valor=array()){
		
        $arrnovedad = array();
		$tmp = new Novedad();
		if (count($valor) > 0){
        	$getrows = $tmp->getRows("SELECT * FROM Novedad WHERE ".$campo." = ?", array($valor));
		}else{
        	$getrows = $tmp->getRows("SELECT * FROM Novedad", $valor);
		}
        foreach ($getrows as $getrow) {
			$ins = new Novedad();
            $ins->idNovedad = $getrow['idNovedad'];
			$ins->repuestoSaliente = $getrow['repuestoSaliente'];
			$ins->fechaNovedad = $getrow['fechaNovedad'];
			$ins->descripcion = $getrow['descripcion'];
			$ins->excedente = $getrow['excedente'];
			$ins->reembolso = $getrow['reembolso'];
			$ins->tipoNovedad = $getrow['tipoNovedad'];
			$ins->repuestoEntrante = Repuesto::buscarForId($getrow['repuestoEntrante']);
			$ins->repuestoSaliente = Repuesto::buscarForId($getrow['repuestoSaliente']);
			$ins->venta = Venta::buscarForId($getrow['venta']);
			
			array_push($arrnovedad, $ins);
        }
        $ins->Disconnect();
        return $arrnovedad;
	}
    
	public function inactivar($id){
	
	}
	
	public function registrar (){
		//var_dump($this);
		try {
			$this->insertRow("INSERT INTO Novedad (idNovedad,tipoNovedad, fechaNovedad,descripcion, excedente,reembolso, repuestoEntrante, repuestoSaliente,venta)
VALUES ('NULL', ?, ?, ?, ?, ?, ?, ?, ? )",
				
				 array( 
				    $this->tipoNovedad,
					$this->fechaNovedad,
					$this->descripcion,
					$this->excedente,
					$this->reembolso,
					
					$this->repuestoEntrante -> getIdRepuesto(),
					$this->repuestoSaliente -> getIdRepuesto(),
					$this->venta-> getIdVenta()
										
				)
			);
			$this->Disconnect();
			return true;
		}catch(Exception $e) { 
			throw new Exception($e->getMessage());
			return false;
		}
	}
	
	public function actualizar (){
		
		try {
			$this->getRow("UPDATE Novedad SET   fechaNovedad = ?, descripcion = ?, excedente = ?, reembolso = ?, repuestoEntrante = ?,repuestoSaliente = ?, venta =?   WHERE idNovedad = ? ", array( 
			       
				   
					$this->fechaNovedad,
					$this->descripcion,
					$this->excedente,
					$this->reembolso,
				    $this->repuestoEntrante -> getIdRepuesto(),
					$this->repuestoSaliente -> getIdRepuesto(),
					$this->venta -> getIdVenta(),
					
				    $this->idNovedad
					
				)
			);
			$this->Disconnect();
			return true;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
			return false;
		}
		
	}
	
}
