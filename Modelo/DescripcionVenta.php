<?php 
require_once ('Conexion.php');
require_once('Venta.php');
require_once('Repuesto.php');

class DescripcionVenta extends Conexion {
	
	private $idDescripcionVenta;
	private $cantidad;
	private $valorUnitario;
	private $descripcion;
	private $valorProducto;
	private $venta;
	private $repuesto;
	
	
		
	public function __construct($DescripcionVenta_data=array()){
        parent::__construct();
		if(count($DescripcionVenta_data)>1){
			foreach ($DescripcionVenta_data as $campo => $valor){
               $this->$campo = $valor;
			}
		}else {
			$this->idDescripcionVenta = "";
			$this->cantidad = "";
			$this->valorUnitario = "";
			$this->descripcion = "";
			$this->valorProducto = "";
			$this->venta = new Venta ();
			$this->repuesto = new Repuesto ();
		    
		}
    }
	
	function __destruct (){ }
	
	public function setIdDescripcionVenta ($idDescripcionVenta){
		$this->idDescripcionVenta = $idDescripcionVenta;
	}
	
	public function getIdDescripcionVenta (){
		return $this->idDescripcionVenta;
	}	
	
	public function setCantidad ($cantidad){
		$this->cantidad = $cantidad;
	}
	
	public function getCantidad (){
		return $this->cantidad;
	}
	
	public function setValorUnitario($valorUnitario){
		$this->valorUnitario = $valorUnitario;
	}
	
	public function getValorUnitario (){
		return $this->valorUnitario;
	}
	
		
	
	public function setDescripcion ($descripcion){
		$this->descripcion = $descripcion;
	}
	
	public function getDescripcion (){
		return $this->descripcion;
	}
	
	public function setValorProducto ($valorProducto ){
		$this->valorProducto  = $valorProducto ;
	}
	
	public function getValorProducto  (){
		return $this->valorProducto ;
	}
	
	public function setRepuesto ($repuesto){
		$this->repuesto = $repuesto;
	}
	
	public function getRepuesto (){
		return $this->repuesto;
	}
	
	public function setVenta ($venta){
		$this->venta = $venta;
	}
	
	public function getVenta (){
		return $this->venta;
	}
	
	
	
	
    public static function buscarForId($id){
		if ($id > 0){
			$ins = new DescripcionVenta();
			$getrow = $ins->getRow("SELECT * FROM DescripcionVenta WHERE idDescripcionVenta =?", array($id));
			$ins->idDescripcionVenta = $getrow['idDescripcionVenta'];
			$ins->cantidad = $getrow['cantidad'];
			$ins->valorUnitario = $getrow['valorUnitario'];
			$ins->descripcion = $getrow['descripcion'];
			$ins->valorProducto = $getrow['valorProducto'];
			$ins->venta = Venta::buscarForId($getrow['venta']);
			$ins->repuesto = Repuesto::buscarForId($getrow['repuesto']);

			$ins->Disconnect();
			return $ins;
		}else{
			return NULL;
		}
		$this->Disconnect();
	}
	
	public static function buscarAll(){
		return DescripcionVenta::buscar("idDescripcionVenta");
	}
	
	public static function buscar($campo, $valor=array()){
        $arrDescripcionVenta = array();
		$tmp = new DescripcionVenta();
		if (count($valor) > 0){
        	$getrows = $tmp->getRows("SELECT * FROM DescripcionVenta WHERE ".$campo." = ?", array($valor));
		}else{
        	$getrows = $tmp->getRows("SELECT * FROM DescripcionVenta", $valor);
		}
        foreach ($getrows as $getrow) {
            $ins = new DescripcionVenta();
			$ins->idDescripcionVenta = $getrow['idDescripcionVenta'];
			$ins->cantidad = $getrow['cantidad'];
			$ins->valorUnitario = $getrow['valorUnitario'];
			$ins->descripcion = $getrow['descripcion'];
			$ins->valorProducto = $getrow['valorProducto'];
			$ins->venta = Venta::buscarForId($getrow['venta']);
			$ins->repuesto = Repuesto::buscarForId($getrow['repuesto']);

			array_push($arrDescripcionVenta, $ins);
			 $ins->Disconnect();
        }
       
        return $arrDescripcionVenta;
	}
    
	
	public function registrar (){
		//var_dump($this);
		try {
			$this->insertRow("INSERT INTO descripcionventa (idDescripcionVenta, cantidad, valorUnitario, descripcion, valorProducto, venta, repuesto) VALUES ('NULL', ?, ?, ?, ?, ?, ?)",
				
				 array( 
					$this->cantidad,
					$this->valorUnitario,
					$this->descripcion,
					$this->valorProducto,
					$this->venta -> getIdVenta(),
					$this->repuesto -> getIdRepuesto(),
					//$this->repuesto -> setEstado('Inactivo')
					
				)
			);
			$this->Disconnect();
			return true;
		}catch(Exception $e) { 
			throw new Exception($e->getMessage());
			return false;
		}
	}
	
	
	
	
}
?>