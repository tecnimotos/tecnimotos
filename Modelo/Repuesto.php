<?php
require_once ('Conexion.php');
require_once('UnidadMedida.php');
require_once('TipoRepuesto.php');


class Repuesto extends Conexion{
	private $idRepuesto;  
	private $marca;
	private $descripcion;
	private $referencia;
	private $stockMinimo;
	private $garantia;
	private $valorBase;
	private $tipoRepuesto;
	private $unidadMedida;
	private $estado;
	private $stockActual;
	private $porcentaje;
	private $valorPorcentaje;
	
	
	public function __construct($Repuesto_data=array()){
        parent::__construct();
		if(count($Repuesto_data)>1){
			foreach ($Repuesto_data as $campo => $valor){
               $this->$campo = $valor;
			}
		}else {
			$this->idRepuesto = "";
			$this->marca = "";
			$this->descripcion="";
			$this->referencia = "";
			$this->stockMinimo = "";
			$this->garantia = "";
			$this->valorBase = "";
			$this->tipoRepuesto=new TipoRepuesto();
			$this->unidadMedida=new UnidadMedida();
			$this->estado = "";
			$this->stockActual = "";
			$this->porcentaje= "";
			$this->valorPorcentaje = "";
			
			
			
			
		}
	}
	
		function __destruct (){ }
		
        
		public function setIdRepuesto ($idRepuesto){
		$this->idRepuesto = $idRepuesto;
	    }
	
	    public function getIdRepuesto(){
		return $this->idRepuesto;
	    }
		
	    public function setMarca ($marca){
		$this->marca = $marca;
	    }
	
	    public function getMarca (){
		return $this->marca;
	    }
		
	    public function setDescripcion ($descripcion){
		$this->descripcion = $descripcion;
	    }
	
	    public function getDescripcion (){
		return $this->descripcion;
	    }
		
	    public function setReferencia ($referencia){
		$this->referencia = $referencia;
	    }
	
	    public function getReferencia (){
		return $this->referencia;
	    }
		
	    public function setStockMinimo ($stockMinimo){
		$this->stockMinimo = $stockMinimo;
	    }
	
	    public function getStockMinimo (){
		return $this->stockMinimo;
	    }
	    public function setGarantia ($garantia){
		$this->ciudad = $garantia;
	    }
	
	    public function getGarantia (){
		return $this->garantia;
	    }
		
	    public function setValorBase ($valorBase){
		$this->valorBase = $valorBase;
	    }
	
	    public function getValorBase (){
		return $this->valorBase;
	    }
		
	    public function setStockActual ($stockActual){
		$this->stockActual = $stockActual;
	    }
	
	    public function getStockActual (){
		return $this->stockActual;
	    }
		
	    
	
	    public function setEstado ($estado){
		$this->estado = $estado;
	    }
	
	    public function getEstado (){
		return $this->estado;
	    }
		
	    public function setTipoRepuesto ($tipoRepuesto){
		$this->tipoRepuesto = $tipoRepuesto;
	    }
	
	    public function getTipoRepuesto (){
		return $this->tipoRepuesto;
	    }
		
	    public function setUnidadMedida ($unidadMedida){
		$this->unidadMedida = $unidadMedida;
	    }
	
	    public function getUnidadMedida (){
		return $this->unidadMedida;
	    } 
		
		public function setPorcentaje ($porcentaje){
		$this->porcentaje = $porcentaje;
	    }
	
	    public function getporcentaje (){
		return $this->porcentaje;
	    } 
		
		public function setValorPorcentaje ($valorPorcentaje){
		$this->valorPorcentaje = $valorPorcentaje;
	    }
	
	    public function getValorPorcentaje (){
		return $this->valorPorcentaje;
	    } 
		
		public static function buscarForId($id){
		if ($id > 0){
			$rep = new Repuesto();
			$getrow = $rep->getRow("SELECT * FROM repuesto WHERE idRepuesto =?", array($id));
			$rep->idRepuesto = $getrow['idRepuesto'];
			$rep->marca = $getrow['marca'];
			$rep->descripcion = $getrow['descripcion'];
			$rep->referencia = $getrow['referencia'];
			$rep->stockMinimo = $getrow['stockMinimo'];
			$rep->garantia = $getrow['garantia'];
		    $rep->valorBase = $getrow['valorBase'];
			$rep->tipoRepuesto = TipoRepuesto::buscarForId($getrow['tipoRepuesto']);
			$rep->unidadMedida = UnidadMedida::buscarForId($getrow['unidadMedida']);
			$rep->estado = $getrow['estado'];
			$rep->stockActual = $getrow['stockActual'];
			$rep->porcentaje = $getrow['porcentaje'];
			$rep->valorPorcentaje = $getrow['valorPorcentaje'];
			
			
			return $rep;
			
		}else{
			return NULL;
		}
		$this->Disconnect();
	}
	
	public static function buscarAll(){
		return Repuesto::buscar("idRepuesto");	
	}
	
	
	public static function buscar($campo, $valor=array()){
        $arrRepuesto = array();
		$tmp = new Repuesto();
		if (count($valor) > 0){
        	$getrows = $tmp->getRows("SELECT * FROM Repuesto WHERE ".$campo." = ?", array($valor));
		}else{
        	$getrows = $tmp->getRows("SELECT * FROM Repuesto", $valor);
		}
        foreach ($getrows as $getrow) {
            $rep = new Repuesto();
			
			$rep->idRepuesto = $getrow['idRepuesto'];
			$rep->marca = $getrow['marca'];
			$rep->descripcion = $getrow['descripcion'];
			$rep->referencia = $getrow['referencia'];
			$rep->stockMinimo = $getrow['stockMinimo'];
			$rep->garantia = $getrow['garantia'];
			$rep->valorBase = $getrow['valorBase'];
			$rep->tipoRepuesto = TipoRepuesto::buscarForId($getrow['tipoRepuesto']);
			$rep->unidadMedida = UnidadMedida::buscarForId($getrow['unidadMedida']);
			$rep->stockActual = $getrow['stockActual'];
			$rep->estado = $getrow['estado'];
			$rep->porcentaje = $getrow['porcentaje'];
			$rep->valorPorcentaje = $getrow['valorPorcentaje'];

            array_push($arrRepuesto, $rep);
			$rep->Disconnect();
        }
        
        return $arrRepuesto;
    }
    
	
    
	public static function buscarForString($campo, $valor=array()){
        $arrRepuesto = array();
		$tmp = new Repuesto();
		if (count($valor) > 0){
        	$getrows = $tmp->getRows("SELECT * FROM Repuesto WHERE ".$campo." LIKE %?%", array($valor));
		}else{
        	$getrows = $tmp->getRows("SELECT * FROM Repuesto", array($valor));
		}
        foreach ($getrows as $getrow) {
            $rep = new Repuesto();

			$rep->idRepuesto = $getrow['idRepuesto'];
			$rep->marca = $getrow['marca'];
			$rep->descripcion = $getrow['descripcion'];
			$rep->referencia = $getrow['referencia'];
			$rep->stockMinimo = $getrow['stockMinimo'];
			$rep->garantia = $getrow['garantia'];
			$rep->valorBase = $getrow['valorBase'];
			$rep->tipoRepuesto = Repuesto::buscarForId($getrow['tipoRepuesto']);
			$rep->unidadMedida = UnidadMedida::buscarForId($getrow['unidadMedida']);
			$rep->porcentaje = $getrow['porcentaje'];
			$rep->valorPorcentaje = $getrow['valorPorcentaje'];

            array_push($arrRepuesto, $rep);
			$per->Disconnect();
        }
        
        return $arrRepuesto;
    }
    public function registrar (){
	     
		try {
			$this->insertRow("INSERT INTO repuesto (idRepuesto, marca, descripcion, referencia, stockMinimo, garantia, valorBase, tipoRepuesto, unidadMedida, estado , stockActual, porcentaje , valorPorcentaje) VALUES ('NULL',?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ? , ?)",        
			 array( 
				    $this->marca,
					$this->descripcion,
					$this->referencia,
					$this->stockMinimo,
					$this->garantia,
					$this->valorBase,
					$this->tipoRepuesto-> getIdTipoRepuesto(),
					$this->unidadMedida->getIdUnidadMedida(),
					$this->estado,
					$this->stockActual,
					$this->porcentaje,
					$this->valorPorcentaje
					
				)
			);
			$this->Disconnect();
			return true;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
			return false;
		}
	}
	public function actualizar (){
			//`idRepuesto`, `marca`, `descripcion`, `referencia`, `stockMinimo`, `garantia`, `valorBase`, `tipoRepuesto`, `unidadMedida`, `estado`, `stockActual`, `fechaVencimiento`

		try {
			$this->updateRow("UPDATE repuesto SET marca = ?, descripcion = ?, referencia = ?, stockMinimo = ?, garantia = ?, valorBase = ? , tipoRepuesto = ? , unidadMedida = ?, estado = ?, stockActual = ?, porcentaje = ? , valorPorcentaje = ? WHERE idRepuesto = ?", array( 
					$this->marca,
					$this->descripcion,
					$this->referencia,
					$this->stockMinimo,
					$this->garantia,
					$this->valorBase,
					$this->tipoRepuesto->getIdTipoRepuesto(),
					$this->unidadMedida->getIdUnidadMedida(),
					$this->estado,
					$this->stockActual,
					$this->porcentaje,
					$this->valorPorcentaje,
					
					$this->idRepuesto
					
				)
				
			);
			
			$this->Disconnect();
			return true;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
			return false;
		}
	}
	 public function inactivar($id){
	
	}
	
}
