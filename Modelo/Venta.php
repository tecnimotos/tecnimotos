<?php
require_once('Conexion.php');
require_once('Persona.php');
require_once('Empleado.php');


class Venta extends Conexion {
	
	private $idVenta;
	private $fechaVenta;
	private $referencia;
	private $vendedor;
	private $cliente;
	private $valorTotal;
	
	public function __construct($venta_data=array()){
        parent::__construct();
		if(count($venta_data)>1){
			foreach ($venta_data as $campo => $valor){
               $this->$campo = $valor;
			}
		}else {
			$this->idVenta = "";
			$this->fechaVenta = "";
			$this->referencia = "";
			$this->vendedor = new Empleado();
			$this->cliente = new Persona();
			$this->valorTotal = "";

		}
    }
	
	function __destruct (){ }
	
	public function setIdVenta ($idVenta){
		$this->idVenta = $idVenta;
	}
	
	public function getIdVenta (){
		return $this->idVenta;
	}	
	
	public function setFechaVenta ($fechaVenta){
		$this->fechaVenta = $fechaVenta;
	}
	
	public function getFechaVenta (){
		return $this->fechaVenta;
	}
		
	
	public function setReferencia ($referencia){
		$this->referencia = $referencia;
	}
	
	public function getReferencia (){
		return $this->referencia;
	}
	
	
	
	public function setVendedor ($vendedor){
		$this->vendedor = $vendedor;
	}
	
	public function getVendedor (){
		return $this->vendedor;
	}
	
	public function setCliente ($cliente){
		$this->cliente = $cliente;
	}
	
	public function getCliente (){
		return $this->cliente;
	}
	
	public function setValorTotal ($valorTotal){
		$this->valorTotal = $valorTotal;
	}
	
	public function getValorTotal (){
		return $this->valorTotal;
	}
	
    public static function buscarForId($id){
		if ($id > 0){
			$vent = new venta();
			$getrow = $vent->getRow("SELECT * FROM Venta WHERE idVenta =?", array($id));
			$vent->idVenta = $getrow['idVenta'];
			$vent->fechaVenta = $getrow['fechaVenta'];
			$vent->referencia = $getrow['referencia'];
			$vent->vendedor = Persona::buscarForId($getrow['vendedor']);
			$vent->cliente = Persona::buscarForId($getrow['cliente']);
			$vent->valorTotal = $getrow['valorTotal'];

			$vent->Disconnect();
			return $vent;
		}else{
			return NULL;
		}
		$this->Disconnect();
	}
	
	public static function buscarAll(){
		return Venta::buscar("idVenta");
	}
	
	
	public static function buscar($campo, $valor=array()){
        $arrventa = array();
		$tmp = new Venta();
		if (count($valor) > 0){
        	$getrows = $tmp->getRows("SELECT * FROM venta WHERE ".$campo." = ?", array($valor));
		}else{
        	$getrows = $tmp->getRows("SELECT * FROM venta", $valor);
		}
        foreach ($getrows as $getrow) {
           $ins = new Venta();
			$ins->idVenta = $getrow['idVenta'];
			$ins->fechaVenta = $getrow['fechaVenta'];
			$ins->referencia = $getrow['referencia'];
			$ins->vendedor = Persona::buscarForId($getrow['vendedor']);
			$ins->cliente = Persona::buscarForId($getrow['cliente']);
			$ins->valorTotal = $getrow['valorTotal'];

            array_push($arrventa, $ins);
			$ins->Disconnect();
        }
        
        return $arrventa;
    }
    
	public static function buscarForString($campo, $valor=array()){
        $arrventa = array();
		$tmp = new Venta();
		if (count($valor) > 0){
        	$getrows = $tmp->getRows("SELECT * FROM venta WHERE ".$campo." LIKE %?%", array($valor));
		}else{
        	$getrows = $tmp->getRows("SELECT * FROM venta", array($valor));
		}
        foreach ($getrows as $getrow) {
            $ins = new Venta();
			$ins->idVenta = $valor['idVenta'];
			$ins->fechaVenta = $valor['fechaVenta'];
			$ins->referencia = $valor['referencia'];	
			$ins->vendedor = Persona::buscarForId($valor['vendedor']);
			$ins->cliente = Persona::buscarForId($valor['cliente']);
			$ins->valorTotal = $valor['valorTotal'];
			$ins->estado = "activo";

            array_push($arrventa, $ins);
			$ins->Disconnect();
        }
        
        return $arrventa;
    }
    
	
	
	public function registrar (){
		//var_dump($this);
		try {
			global $idVenta;
			$this->idVenta = $this->insertRow("INSERT INTO Venta (idVenta, fechaVenta, referencia, vendedor, cliente, valorTotal) VALUES ('NULL', ?, ?, ?, ?, ?)",
				
				 array( 
					$this->fechaVenta,
					$this->referencia,
					$this->vendedor-> getIdPersona(),
					$this->cliente -> getIdPersona(),
					$this->valorTotal
					
				)
			);
			$this->Disconnect();
			return true;
		}catch(Exception $e) { 
			throw new Exception($e->getMessage());
			return false;
		}
	}
	
	public function actualizar (){
		var_dump($this);
		try {
		
			$this->updateRow("UPDATE venta SET fechaVenta = ?, referencia = ?,  vendedor = ?, cliente = ?, valorTotal = ? WHERE idVenta = ?", array( 
			
				 	$this->fechaVenta,
					$this->referencia,
					$this->vendedor->getIdPersona(),
					$this->cliente ->getIdPersona(),
					$this->valorTotal,
					$this->idVenta
				
				)
			);
			$this->Disconnect();
			return true;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
			return false;
		}
		
	}

}
?>