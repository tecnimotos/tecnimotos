<?php
require_once ('Conexion.php');
require_once('Proveedor.php');
require_once('Persona.php');

class Pedido extends Conexion{
	
	private $idPedido;  
	private $fecha;
	private $referencia;
	private $proveedor;
	private $persona;

	public function __construct($Pedido_data=array()){
        parent::__construct();
		if(count($Pedido_data)>1){
			foreach ($Pedido_data as $campo => $valor){
               $this->$campo = $valor;
			}
		}else {
			$this->idPedido = "";
			$this->fecha = "";
			$this->referencia="";
			
			$this->proveedor = new Proveedor();
			$this->persona = new Persona();
			
		}
		}
		function __destruct (){ }
		
        
		public function setIdPedido ($idPedido){
		$this->idPedido = $idPedido;
	    }
	
	    public function getIdPedido (){
		return $this->idPedido;
	    }
		
		public function setFecha ($fecha){
		$this->fecha = $fecha;
	    }
	
	    public function getFecha (){
		return $this->fecha;
	    }
		public function setReferencia ($referencia){
		$this->referencia = $referencia;
	    }
	
	    public function getReferencia (){
		return $this->referencia;
	    }
		
		
		
		 public function setProveedor ($proveedor){
		$this->proveedor = $proveedor;
	    }
	
	    public function getProveedor (){
		return $this->proveedor;
	    }
		
	    public function setPersona ($persona){
		$this->persona = $persona;
	    }
	
	    public function getPersona (){
		return $this->persona;
	    } 
		
		
		
		public static function buscarForId($id){
		if ($id > 0){
			$ped = new Pedido();
			$getrow = $ped->getRow("SELECT * FROM Pedido WHERE idPedido = ?", array($id));
			$ped->idPedido = $getrow['idPedido'];
			$ped->fecha = $getrow['fecha'];
			$ped->referencia = $getrow['referencia'];
		
			$ped->proveedor = Proveedor::buscarForId($getrow['proveedor']);
			$ped->persona = Persona::buscarForId($getrow['persona']);
			
			return $ped;
			
		}else{
			return NULL;
		}
		$this->Disconnect();
	}
	
	public static function buscarAll(){
		return Pedido::buscar("idPedido");	
	}
	
	public static function buscar($campo, $valor=array()){
        $arrPedido = array();
		$tmp = new Pedido();
		if (count($valor) > 0){
        	$getrows = $tmp->getRows("SELECT * FROM Pedido WHERE ".$campo." = ?", array($valor));
		}else{
        	$getrows = $tmp->getRows("SELECT * FROM Pedido", $valor);
		}
        foreach ($getrows as $getrow) {
            $ped = new Pedido();
			
			$ped->idPedido = $getrow['idPedido'];
			$ped->fecha = $getrow['fecha'];
			$ped->referencia = $getrow['referencia'];
			
			$ped->proveedor = Proveedor::buscarForId($getrow['proveedor']);
			$ped->persona = Persona::buscarForId($getrow['persona']);
            
            array_push($arrPedido, $ped);
			$ped->Disconnect();
        }
        
        return $arrPedido;
    }
	
	
	
	public function actualizar (){
		
		try {
			$this->updateRow("UPDATE Pedido SET fecha = ?,  referencia = ?, proveedor = ?, persona = ?  WHERE idPedido = ?", array( 
					$this->fecha,
					$this->referencia,
					
					$this->proveedor-> getIdProveedor(),
					$this->persona->getIdPersona(),
					$this->idPedido
					
				)
				
			);
			
			$this->Disconnect();
			return true;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
			return false;
		}
	}
	 public function inactivar($id){
	
	}
	public function registrar (){
	     
		try {
			$this->insertRow("INSERT INTO Pedido (idPedido,fecha,referencia,proveedor,persona) VALUES ('NULL',?, ?, ?, ?)",        
			 array( 
				    $this->fecha,
					$this->referencia,
					
					$this->proveedor->getIdProveedor(),
					$this->persona->getIdPersona()
						
				)
			);
			$this->Disconnect();
			return true;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
			return false;
		}
	}
	
	static public function rowwPedido($name){
		try {
			
			$arrPedido = ctrPedido::buscar('referencia',$name);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th>Fecha</th>";
			            $text .= " <th class='hidden-xs'>Referencia</th>";
						   $text .= " <th class='hidden-xs'>Proveedor</th>";
			            $text .= " <th class='hidden-xs'>Empleado</th>";
						
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <tbody>";
			foreach ($arrPedido as $pedido){
			
					$text .= "<tr class='success'>";
					$text .= "      <td class='center'>".$pedido->getIdPedido()."</td>";
					$text .= "      <td class='center'>".$pedido->getFecha()."</td>";
					$text .= "      <td >".$pedido->getReferencia()."</td>";
					$text .= "      <td class='hidden-xs'>".$pedido->getProveedor()->getRazonSocial()."</td>";
					$text .= "      <td class='hidden-xs'>".$pedido->getPersona()->getNombres()."</td>";
					
				
					$text .= "		<td class='center'> ";

					$text .= "		</td>";
					$text .= "</tr>";
			}
					
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/ConsultarPedido.php?respuesta=error");
		}
	}

	}

	?>