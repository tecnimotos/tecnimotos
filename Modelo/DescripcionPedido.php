<?php
require_once ('Conexion.php');
require_once('Repuesto.php');
require_once('Pedido.php');

class DescripcionPedido extends Conexion{
	
	private $idDescripcionPedido;  
	private $cantidad;
	
	private $ubicacion;
	private $valorUnitario;
	private $repuesto;
	private $pedido;
	
	
	public function __construct($Repuesto_data=array()){
        parent::__construct();
		if(count($Repuesto_data)>1){
			foreach ($Repuesto_data as $campo => $valor){
               $this->$campo = $valor;
			}
		}else {
			$this->idDescripcionPedido = "";
			$this->cantidad = "";
			
			$this->ubicacion = "";
			$this->valorUnitario = "";
			$this->repuesto = new Repuesto();
			$this->pedido = new Pedido();
			
    }
	}
	
	function __destruct (){ }
		
        
	
	public function setIdDescripcionPedido ($idDescripcionPedido){
		$this->idDescripcionPedido = $idDescripcionPedido;
	    }
	
	    public function getIdDescripcionPedido (){
		return $this->idDescripcionPedido;
	    }
		
	    public function seCantidad ($cantidad){
		$this->cantidad = $cantidad;
	    }
	
	    public function getCantidad (){
		return $this->cantidad;
	    }
		
	    
		
	    public function setUbicacion ($ubicacion){
		$this->ubicacion = $ubicacion;
	    }
	
	    public function getUbicacion (){
		return $this->ubicacion;
	    }
		
	    public function setValorUnitario ($valorUnitario){
		$this->valorUnitario = $valorUnitario;
	    }
	
	    public function getValorUnitario (){
		return $this->valorUnitario;
	    }
	
	  
		
	    public function setRepuesto ($repuesto){
		$this->repuesto = $repuesto;
	    }
	
	    public function getRepuesto (){
		return $this->repuesto;
	    }
		
	    public function setPedido ($pedido){
		$this->pedido = $pedido;
	    }
	
	    public function getPedido (){
		return $this->pedido;
	    } 
		
		
		
		public static function buscarForId($id){
		if ($id > 0){
			$des = new DescripcionPedido();
			$getrow = $des->getRow("SELECT * FROM DescripcionPedido WHERE idDescripcionPedido =?", array($id));
			
			$des->idDescripcionPedido = $getrow['idDescripcionPedido'];
			$des->cantidad = $getrow['cantidad'];
			
			$des->ubicacion = $getrow['ubicacion'];
			$des->valorUnitario = $getrow['valorUnitario'];
			
			$des->repuesto = Repuesto::buscarForId($getrow['repuesto']);
			$des->pedido = Pedido::buscarForId($getrow['pedido']);
			
			
			return $des;
			
		}else{
			return NULL;
		}
		$this->Disconnect();
	}
	
		public static function buscarAll(){
		return DescripcionPedido::buscar("idDescripcionPedido");	
	}
	
	public static function buscar($campo, $valor=array()){
        $arrDescripcionPedido = array();
		$tmp = new DescripcionPedido();
		if (count($valor) > 0){
        	$getrows = $tmp->getRows("SELECT * FROM DescripcionPedido WHERE ".$campo." = ?", array($valor));
		}else{
        	$getrows = $tmp->getRows("SELECT * FROM DescripcionPedido", $valor);
		}
        foreach ($getrows as $getrow) {
            $des = new DescripcionPedido();
			
			$des->idDescripcionPedido = $getrow['idDescripcionPedido'];
			$des->cantidad = $getrow['cantidad'];
		
			$des->ubicacion = $getrow['ubicacion'];
			$des->valorUnitario = $getrow['valorUnitario'];
		
			$des->repuesto = Repuesto::buscarForId($getrow['repuesto']);
			$des->pedido = Pedido::buscarForId($getrow['pedido']);
            
            array_push($arrDescripcionPedido, $des);
			$des->Disconnect();
        }
        
        return $arrDescripcionPedido;
    }
	public function registrar (){
	     
		try {
			
			$this->insertRow("INSERT INTO descripcionpedido (idDescripcionPedido,cantidad, ubicacion, valorUnitario, repuesto, pedido ) VALUES ('NULL',?, ?, ?, ?, ?)",        
			 array( 
				    $this->cantidad,
					$this->ubicacion,
					$this->valorUnitario,
					$this->repuesto-> getIdRepuesto(),
					$this->pedido->getIdPedido(),
					
				)
			);
			$this->Disconnect();
			return true;
	}catch(Exception $e) {
			throw new Exception($e->getMessage());
			return false;
		}
	}
	
	
	/*public static function buscarForString($campo, $valor=array()){
         $arrdespedido  = array();
		$tmp = new DescripcionPedido();
		if (count($valor) > 0){
        	$getrows = $tmp->getRows("SELECT * FROM descripcionPedido WHERE ".$campo." LIKE %?%", array($valor));
		}else{
        	$getrows = $tmp->getRows("SELECT * FROM descripcionPedido", array($valor));
		}
        foreach ($getrows as $getrow) {
            $ins = new DescripcionPedido();
			$ins->idDescripcionPedido = $valor['idDescripcionPedido'];
			$ins->cantidad= $valor['cantidad'];
			$ins->porcentaje= $valor['porcentaje'];
			$ins->ubicacion= $valor['ubicacion'];
			$ins->valorUnitario= $valor['porcentaje'];
			$ins->repuesto = Repuesto::buscarForId($valor['repuesto']);
			$ins->pedido = Pedido::buscarForId($valor['pedido']);
			

            array_push( $arrdespedido , $ins);
			$ins->Disconnect();
        }
        
        return  $arrdespedido ;
    }*/
}

