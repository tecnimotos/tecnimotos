<?php

require_once('../Modelo/DescripcionPedido.php');


if (!session_id())session_start();
if(!empty($_GET['action'])){
	ctrDescripcionPedido::main($_GET['action']);
}
	

	class ctrDescripcionPedido{
	
	static function main($action){
		$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrDescripcionPedido::registrar();
		}else if ($action == "buscarID"){
			ctrDescripcionPedido::buscarID($_POST['idDescripcionPedido']);
		}else if ($action == "actualizar"){
			ctrDescripcionPedido::actualizar();
		}
	}
	 static public function buscarID ($id){
		try {
			return DescripcionPedido::buscarForId($id);
		} catch (Exception $e) {
		//	header("");
		}
		
	}
	static public function buscarAll (){
		try {
			return DescripcionPedido::buscarAll();
		} catch (Exception $e) {
			header("");
		}
	}
	static public function buscar ($campo, $parametro){
		try {
			return DescripcionPedido::buscar($campo, $parametro);
		} catch (Exception $e) {
			header("");
		}
	}
	
	
	static public function registrar (){
		try {
			
			$arrayDescripcionPedido = array();
			$arrayDescripcionPedido['cantidad'] = $_POST['cantidad'];
			$arrayDescripcionPedido['ubicacion'] = $_POST['ubicacion'];
			$arrayDescripcionPedido['valorUnitario'] = $_POST['valorUnitario'];
		
			$arrayDescripcionPedido['repuesto'] = Repuesto::buscarForId($_POST['idRepuesto']);
			$arrayDescripcionPedido['pedido'] = Pedido::buscarForId($_POST['idPedido']);
			
			$DescripcionPedido = new DescripcionPedido ($arrayDescripcionPedido);
			
			
			if ( $DescripcionPedido->registrar()){
				$arrayDescripcionPedido['repuesto'] -> setStockActual(
							$arrayDescripcionPedido['repuesto'] -> getStockActual() + 
							$arrayDescripcionPedido ['cantidad']
							);
				$arrayDescripcionPedido['repuesto'] -> actualizar();
				
				}
                header("Location: ../Vista/RegistrarDescripcionPedido.php?respuesta=correcto");
		} catch (Exception $e) {
			
			header("Location: ../Vista/BuscarDescripcionPedido.php?respuesta=correcto");
		
		}
		
			
	}
	
	
	
	
	
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$arrDescripcionPedido = ctrDescripcionPedido::buscarAll();
			$text .= "<option selected value='0'>Seleccione una opción</option>";
			foreach ($arrDescripcionPedido as $DescripcionPedido){
				$text .= "<option value=".$DescripcionPedido->getIdDescripcionPedido().">".$DescripcionPedido->getIdDescripcionPedido()." ".$DescripcionPedido->getCantidad()."</option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowsDescripcionPedido (){
		try {
			$arrPedido = ctrDescripcionPedido::buscarAll();
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th class='center'> Cantidad</th>";
			            $text .= " <th> Ubicación</th>";
						$text .= " <th class='center'> Valor Unitario</th>";
			            $text .= " <th class='hidden-xs'>Repuesto</th>";
			            $text .= " <th class='center'> Pedido</th>";
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrPedido as $pedido){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$pedido->getIdDescripcionPedido()."</td>";
					$text .= "      <td class='center'>".$pedido->getCantidad()."</td>";
					$text .= "      <td> ".$pedido->getUbicacion()."</td>";
					$text .= "      <td class='center'>".$pedido->getValorUnitario()."</td>";
					$text .= "      <td> ".$pedido->getRepuesto()->getReferencia()."</td>";
					$text .= "      <td class='center'> ".$pedido->getPedido()->getReferencia()."</td>";
					
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							   
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	
	
	static public function rowwDesPedido($name){
		try {
			
			$arrPedido = ctrDescripcionPedido::buscar('ubicacion',$name);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th class='center'>Cantidad</th>";
						   $text .= " <th class='hidden-xs'>Ubicación</th>";
			            $text .= " <th class='hidden-xs'>Valor Unitario</th>";
						$text .= " <th class='hidden-xs'>Repuesto</th>";
			            $text .= " <th class='hidden-xs'>Pedido</th>";
						
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <tbody>";
			foreach ($arrPedido as $pedido){
			
					if ($_SESSION['cont'] == 1){
						$text .= "<tr class='success'>";
					}else if ($_SESSION['cont'] == 2){
						$text .= "<tr class='active'>";
					}else if ($_SESSION['cont'] == 3){
						$text .= "<tr class='info'>";
					}else if ($_SESSION['cont'] == 4){
						$text .= "<tr class='warning'>";
					}else if ($_SESSION['cont'] == 5){
						$text .= "<tr class='danger'>";
					}
					$text .= "      <td class='center'>".$pedido->getIdDescripcionPedido()."</td>";
					$text .= "      <td class='center'>".$pedido->getCantidad()."</td>";
					$text .= "      <td >".$pedido->getUbicacion()."</td>";
					$text .= "      <td >".$pedido->getValorUnitario()."</td>";
					$text .= "      <td class='hidden-xs'>".$pedido->getRepuesto()->getReferencia()."</td>";
					$text .= "      <td class='hidden-xs'>".$pedido->getPedido()->getReferencia()."</td>";
					
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							   
							    
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					
					$text .= "</tr>";
			}
			if($_SESSION['cont'] >= 5) {
				$_SESSION['cont'] = 0;
			}		
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/ConsultarDescripcionPedido.php?respuesta=error");
		}
	}
	
	}
?>