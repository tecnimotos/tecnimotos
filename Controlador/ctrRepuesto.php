<?php
//session_start();
require_once('../Modelo/Repuesto.php');
if (!session_id())session_start();

if(!empty($_GET['action'])){
	ctrRepuesto::main($_GET['action']);
	
}

	class ctrRepuesto{
	
	static function main($action){
		$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrRepuesto::registrar();
		}else if ($action == "buscarID"){
			ctrRepuesto::buscarID($_POST['idRepuesto']);
		}else if ($action == "actualizar"){
			ctrRepuesto::actualizar();
		}else if ($action == "inactivar"){
			ctrRepuesto::inactivar($_SESSION['idRep']);
		}else if ($action == "activar"){
			ctrRepuesto::activar($_SESSION['idRep']);
		}
	

}
        static public function buscarID ($id){
		try {
			return Repuesto::buscarForId($id);
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function buscarAll (){
		try {
			return Repuesto::buscarAll();
		} catch (Exception $e) {
			header("");
		}
	}

	static public function buscar ($campo, $parametro){
		try {
			return Repuesto::buscar($campo, $parametro);
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function TexboxRepuesto($id){
		try {
			$arrRepuesto = array();
			$arrRepuesto = ctrRepuesto::buscarID($id);
			
			$text ="<div class='form-group'>";
			$text .="<label class='control-label'>Marca <span class='symbol required'></span></label>";
			$text .= "<input type='text' placeholder='Ingrese el marca del Repuesto' class='form-control' id='marca' name='marca' value='".$arrRepuesto->getMarca()."'>";
			$text .="</div >";
			 
			$text .="<div class='form-group'>";
			$text .="<label class='control-label'>Descripcion <span class='symbol required'></span></label>";
			$text .="<input type='text' placeholder='Ingrese la descripcion' class='form-control' id='descripcion' name='descripcion' value='".$arrRepuesto->getDescripcion()."'>";
			$text .="</div>";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'> Referencia <span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese la referencia' class='form-control' id='referencia' name='referencia' value='".$arrRepuesto->getReferencia()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>stockMinimo<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese el stockMinimo' class='form-control' id='stockMinimo' name='stockMinimo' value='".$arrRepuesto->getStockMinimo()."'>";
			$text .="</div >";
			
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>garantia<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese la garantia' class='form-control' id='garantia' name='garantia' value='".$arrRepuesto->getGarantia()."'>";
			$text .="</div >";
			
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>valorBase<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese el valor Base' class='form-control' id='valorBase' name='valorBase' value='".$arrRepuesto->getValorBase()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>tipoRepuesto<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese el tipo de repuesto' class='form-control' id='idTipoRepuesto' name='idTipoRepuesto' value='".$arrRepuesto->getTipoRepuesto()->getIdTipoRepuesto()."١ ".$arrRepuesto->getTipoRepuesto()->getNombre()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>unidadMedida<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese la unidad de medida' class='form-control' id='idUnidadMedida' name='idUnidadMedida' value='".$arrRepuesto->getUnidadMedida()->getIdUnidadMedida()."١ ".$arrRepuesto->getUnidadMedida()->getNombre()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>estado<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese el estado' class='form-control' id='estado' name='estado' value='".$arrRepuesto->getPorcentaje()."'>";
			$text .="</div >";
			
			
			
			
			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarRepuesto.php?respuesta=error");
		}
	}
	
	static public function inactivar($idRep){
		try {  
			$uni = new Repuesto();
			$uni = Repuesto::buscarForId($idRep);
			$uni->setEstado('Inactivo');
			$uni->actualizar();
			
			header("Location: ../Vista/BuscarRepuesto.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarRepuesto.php?respuesta=error&idRep=".$idRep."");
		}
	}
	
	static public function activar($idRep){
		try {  
			$uni = new Repuesto();
			$uni = Repuesto::buscarForId($idRep);
			$uni->setEstado('Activo');
			$uni->actualizar();
			
			header("Location: ../Vista/BuscarRepuesto.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/ActivarRepuesto.php?respuesta=error&idRep=".$idRep."");
		}
	}
	
	static public function registrar (){
		try {
			$arrayRepuesto = array();
			$arrayRepuesto['marca'] = $_POST['marca'];
			$arrayRepuesto['descripcion'] = $_POST['descripcion'];
			$arrayRepuesto['referencia'] = $_POST['referencia'];
			$arrayRepuesto['stockMinimo'] = $_POST['stockMinimo'];
			$arrayRepuesto['garantia'] = $_POST['garantia'];
			$arrayRepuesto['valorBase'] = $_POST['valorBase'];
			$arrayRepuesto['tipoRepuesto'] = TipoRepuesto::buscarForId($_POST['idTipoRepuesto']);
			$arrayRepuesto['unidadMedida'] = UnidadMedida::buscarForId($_POST['idUnidadMedida']);
			$arrayRepuesto['estado'] = "Activo";
			$arrayRepuesto['stockActual'] = null;
			$arrayRepuesto['porcentaje'] = $_POST['porcentaje'];
			$arrayRepuesto['valorPorcentaje'] = $_POST['valorPorcentaje'];
			

			
			$Repuesto = new Repuesto ($arrayRepuesto);
			$Repuesto->registrar();
			header("Location: ../Vista/BuscarRepuesto.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/Index.php?respuesta=error");
		}
	}
	static public function actualizar (){
		try {
			$arrayRepuesto = array();
			$arrayRepuesto['marca'] = $_POST['marca'];
			$arrayRepuesto['descripcion'] = $_POST['descripcion'];
			$arrayRepuesto['referencia'] = $_POST['referencia'];
			$arrayRepuesto['stockMinimo'] = $_POST['stockMinimo'];
			$arrayRepuesto['garantia'] = $_POST['garantia'];
			$arrayRepuesto['valorBase'] = $_POST['valorBase'];
			$arrayRepuesto['tipoRepuesto'] = TipoRepuesto::buscarForId($_POST['idTipoRepuesto']);
			$arrayRepuesto['unidadMedida'] = UnidadMedida::buscarForId($_POST['idUnidadMedida']);
			$arrayRepuesto['estado'] = $_POST['estado'];
			$arrayRepuesto['stockActual'] = $_POST['stockActual'];
			$arrayRepuesto['porcentaje'] = $_POST['porcentaje'];
			$arrayRepuesto['valorPorcentaje'] = $_POST['valorPorcentaje'];
			
			$arrayRepuesto['idRepuesto'] = $_POST['idRepuesto'];
			
			$Repuesto = new Repuesto ($arrayRepuesto);
			$Repuesto->actualizar();
			header("Location: ../Vista/BuscarRepuesto.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/ModificarRepuesto.php?respuesta=error");
		}
	}
	
	static public function getUpdate ($name,$id){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$Repuesto = ctrRepuesto::buscarID($id);
			
			$text .= "<option value=".$Repuesto->getIdRepuesto().">".$Repuesto->getIdRepuesto()."</option>";
			
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$arrRepuesto = ctrRepuesto::buscar('estado', 'Activo');
			$text .= "<option selected value='0'>Seleccione una opción</option>";
			foreach ($arrRepuesto as $Repuesto){
				$text .= "<option value=".$Repuesto->getIdRepuesto()."> ".$Repuesto->getReferencia()." </option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowRepuesto($idRep){
		try {
			$arrRepuesto = array();
			$arrRepuesto = ctrRepuesto::buscarID($idRep);
			//var_dump($arrRepuesto);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Marca </th>";
			            $text .= " <th class='hidden-xs'>Descripción</th>";
			            $text .= " <th class='hidden-xs'>Referencia</th>";
						$text .= " <th>Valor Base</th>";
						$text .= " <th>tipo Repuesto</th>";
						$text .= " <th>Unidad de Medida</th>";
						$text .= " <th>stockActual</th>";
						$text .= " <th>Porcentaje</th>";
						$text .= " <th>Valor Porcentaje</th>";
					
			    $text .= " </tr>";
	    	$text .= "</thead>";
			//`idRepuesto`, `marca`, `descripcion`, `referencia`, `stockMinimo`, `garantia`, `valorBase`, `tipoRepuesto`, `unidadMedida`, `estado`, `stockActual`, `fechaVencimiento`
			$text .= "  <body>";
						$text .= "      <td class='center'>".$arrRepuesto->getIdRepuesto()."</td>";
					$text .= "      <td class='hidden-xs'>".$arrRepuesto->getMarca()."</td>";
					$text .= "      <td >".$arrRepuesto->getDescripcion()."</td>";
					$text .= "      <td> ".$arrRepuesto->getReferencia()."</td>";
					$text .= "		<td > ".$arrRepuesto->getValorBase()."</td>";
					$text .= "		<td > ".$arrRepuesto->getTipoRepuesto()->getNombre()."</td>";
					$text .= "		<td > ".$arrRepuesto->getUnidadMedida()->getNombre()."</td>";
					$text .= "		<td > ".$arrRepuesto->getStockActual()."</td>";
					$text .= "		<td > ".$arrRepuesto->getPorcentaje()."</td>";
					$text .= "		<td > ".$arrRepuesto->getValorPorcentaje()."</td>";
					
					
		//getIdRepuesto getMarca getDescripcion getReferencia getStockMinimo getGarantia getValorBase getTipoRepuesto getUnidadMedida getEstado getStockActual getFechaVencimiento 

					$text .= "</tr>";
					
			$text .= "  </body>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarTipoRepuesto.php?respuesta=error");
		}
	}
	
	static public function rowsRepuesto ($name){
		try {
			$arrRepuesto = ctrRepuesto::buscarAll ();
			//var_dump($arrRepuesto);
			
			
			
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th > Marca </th>";
			            $text .= " <th > Referencia</th>";
						$text .= " <th >Valor Base</th>";
						$text .= " <th >Tipo de Repuesto</th>";
						$text .= " <th >Unidad de Medida</th>";
						$text .= " <th >StockActual</th>";
						$text .= " <th >Porcentaje</th>";
						$text .= " <th >Valor Porcentaje</th>";
						
						
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrRepuesto as $repuesto){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$repuesto->getIdRepuesto()."</td>";
					$text .= "      <td class='hidden-xs'>".$repuesto->getMarca()."</td>";
					$text .= "      <td> ".$repuesto->getReferencia()."</td>";
					$text .= "      <td> ".$repuesto->getValorBase()."</td>";
					$text .= "      <td> ".$repuesto->getTipoRepuesto()->getNombre()."</td>";
					$text .= "      <td> ".$repuesto->getUnidadMedida()->getNombre()."</td>";
					$text .= "      <td> ".$repuesto->getStockActual()."</td>";
					$text .= "      <td> ".$repuesto->getPorcentaje()."</td>";
					$text .= "      <td> ".$repuesto->getValorPorcentaje()."</td>";
					
					
					
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= '../Vista/ModificarRepuesto.php?idRep=".$repuesto->getIdRepuesto()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Editar'><i class='fa fa-edit'></i></a>";
							    $text .= " <a href= '../Vista/ActivarRepuesto.php?idRep=".$repuesto->getIdRepuesto()."' class= 'btn btn-xs btn-green tooltips' data-placement= 'top' data-original-title= 'Activar'><i class='fa fa-share'></i></a>";
							    $text .= " <a href= '../Vista/InactivarRepuesto.php?idRep=".$repuesto->getIdRepuesto()."' class= 'btn btn-xs btn-red tooltips' data-placement= 'top' data-original-title= 'Inactivar'><i class='fa fa-times fa fa-white'></i></a>";
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Editar";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Activar";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowwRepuesto($name){
		try {
			
			$arrRepuesto = ctrRepuesto::buscar('referencia',$name);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			//var_dump($arrRepuesto);
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Marca </th>";
			            $text .= " <th class='hidden-xs'>Descripción</th>";
			            $text .= " <th class='hidden-xs'>Referencia</th>";
			            $text .= " <th class='hidden-xs'>Valor base</th>";
						$text .= " <th> Tipo Repuesto </th>";
			            $text .= " <th class='hidden-xs'>Unidad Medida</th>";
						$text .= " <th class='hidden-xs'>StockActual</th>";
						$text .= " <th class='hidden-xs'>Porcentaje</th>";
						$text .= " <th class='hidden-xs'>Valor Porcentaje</th>";
			            
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <tbody>";
			if($_SESSION['cont'] > 5) {
				$_SESSION['cont'] = 1;
			}
			foreach ($arrRepuesto as $repuesto){
			
					if ($_SESSION['cont'] == 1){
						$text .= "<tr class='success'>";
					}else if ($_SESSION['cont'] == 2){
						$text .= "<tr class='active'>";
					}else if ($_SESSION['cont'] == 3){
						$text .= "<tr class='info'>";
					}else if ($_SESSION['cont'] == 4){
						$text .= "<tr class='warning'>";
					}else if ($_SESSION['cont'] == 5){
						$text .= "<tr class='danger'>";
					}
					$text .= "      <td class='center'>".$repuesto->getIdRepuesto()."</td>";
					$text .= "      <td class='hidden-xs'>".$repuesto->getMarca()."</td>";
					$text .= "      <td >".$repuesto->getDescripcion()."</td>";
					$text .= "      <td> ".$repuesto->getReferencia()."</td>";
					$text .= "      <td> ".$repuesto->getValorBase()."</td>";
					$text .= "		<td > ".$repuesto->getTipoRepuesto()->getNombre()."</td>";
					$text .= "      <td> ".$repuesto->getUnidadMedida()->getNombre()."</td>";
					$text .= "      <td >".$repuesto->getStockActual()."</td>";
					$text .= "      <td >".$repuesto->getPorcentaje()."</td>";
					$text .= "      <td >".$repuesto->getValorPorcentaje()."</td>";
					
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= '../Vista/ModificarRepuesto.php?idRep=".$repuesto->getIdRepuesto()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Editar'><i class='fa fa-edit'></i></a>";
							    $text .= " <a href= '../Vista/ActivarRepuesto.php?idRep=".$repuesto->getIdRepuesto()."' class= 'btn btn-xs btn-green tooltips' data-placement= 'top' data-original-title= 'Activar'><i class='fa fa-share'></i></a>";
							    $text .= " <a href= '../Vista/InactivarRepuesto.php?idRep=".$repuesto->getIdRepuesto()."' class= 'btn btn-xs btn-red tooltips' data-placement= 'top' data-original-title= 'Inactivar'><i class='fa fa-times fa fa-white'></i></a>";
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Editar";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Activar";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
			}
					
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarTipoRepuesto.php?respuesta=error");
		}
	}
	
	
	static public function TexboxRepuestos($id){
		try {
			$arrRepuesto = array();
			$arrRepuesto = ctrRepuesto::buscarID($id);
			
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>valorBase<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese el valor Base' class='form-control' id='valorBase' name='valorBase' value='".$arrRepuesto->getValorBase()."'>";
			$text .="</div >";
			
			
		
			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarRepuesto.php?respuesta=error");
		}
	}
	
	}
	

?>