<?php 
if (!session_id())session_start();
require_once('../Modelo/Empleado.php');

if(!empty($_GET['action'])){
	
	ctrEmpleado::main($_GET['action']);
	
}
class ctrEmpleado{
	
	static function main($action){
		//$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrEmpleado::registrar();
		}else if ($action == "buscarID"){
			ctrEmpleado::buscarForId($_POST['idPersona']);
		}else if ($action == "actualizar"){
			ctrEmpleado::actualizar($_POST['idPersona']);
		}else if ($action == "BuscarAll"){
			ctrEmpleado::buscarAll($_POST['BuscarAll']);
		}else if ($action == "Buscar"){
			ctrEmpleado::buscar($_POST['Buscar']);
		//}else if ($action == "inactivar"){
			//ctrEmpleado::inactivar($_SESSION['idVen']);
		//}else if ($action == "activar"){
			//ctrEmpleado::activar($_SESSION['idVen']);
		}else if ($action == "login"){
			ctrEmpleado::login ($_POST);
		}
	}
	
	static public function login (){
		$usuario = $_POST['usuario'];
		$contrasena = $_POST['contrasena'];
		$empleado = new Empleado ();
		$state = $empleado->login($usuario,$contrasena);
		if(is_object($state)) {
             $_SESSION ['usuario'] = $state;
             $_SESSION ['idPersona'] = $state->getIdPersona();
            header("Location: ../Vista/Index.php?respuesta=correcto");
		} else if($state == 2){
			//header("Location: ../Vista/Login.php?respuesta=errorContrasena");
		}else{
			//header("Location: ../Vista/Login.php?respuesta=errorGeneral");
		}

	}

	static public function registrar (){
	try {
			$arrayPersonas = array();
			$arrayPersonas['tipoUsuario'] = $_POST['tipoUsuario'];
			$arrayPersonas['identificacion'] = $_POST['identificacion'];
			$arrayPersonas['nombres'] = $_POST['nombres'];
			$arrayPersonas['apellidos'] = $_POST['apellidos'];
			$arrayPersonas['direccion'] = $_POST['direccion'];
			$arrayPersonas['telefono'] = $_POST['telefono'];
			$arrayPersonas['email'] = $_POST['email'];
			$arrayPersonas['usuario'] = $_POST['usuario'];
			$arrayPersonas['contrasena'] = $_POST['contrasena'];
			$arrayPersonas['estado'] = "Activo";
			
			$Empleado = new Empleado ($arrayPersonas);
			$Empleado->registrar();
		header("Location: ../Vista/Index.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: /Vista/RegistrarPersona.php?respuesta=error");
	}
	}
	static public function actualizar (){
		try {
			$arrayPersonas = array();
			
			$arrayPersonas = array();
			$arrayPersonas['identificacion'] = $_POST['identificacion'];
			$arrayPersonas['nombres'] = $_POST['nombres'];
			$arrayPersonas['apellidos'] = $_POST['apellidos'];
			$arrayPersonas['direccion'] = $_POST['direccion'];
			$arrayPersonas['telefono'] = $_POST['telefono'];
			$arrayPersonas['email'] = $_POST['email'];
			$arrayPersonas['tipoUsuario'] = $_POST['tipoUsuario'];
			$arrayPersonas['usuario'] = $_POST['usuario'];
			$arrayPersonas['contrasena'] = md5($_POST['contrasena']);
		    $arrayPersonas['estado'] = $_POST['estado'];
			$arrayPersonas['idEmpleado'] = $_POST['idPersona'];
			$Empleado = new Empleado ($arrayPersonas);
			$Empleado->actualizar();
			header("Location: ../Vista/BuscarVendedor.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/ActualizarVendedor.php?respuesta=error");
		}
	}
	
	static public function buscarID ($id){
		try {
			return Empleado::buscarForId($id);
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function buscarAll (){
		try {
			return Empleado::buscarAll();
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function buscarForString ($campo, $parametro){
		try {
			return Empleado::buscarForString($campo, $parametro);
		} catch (Exception $e) {
			return false;
		}
	}

	static public function buscar ($campo, $parametro){
		try {
			return Empleado::buscar($campo, $parametro);
			header("Error al buscar");
		} catch (Exception $e) {
			return false;
		}
	}
	
	static public function TexboxVendedor($id){
		try {
			$arrVendedor = array();
			$arrVendedor = ctrEmpleado::buscarID($id);
			//var_dump($arrVendedor);
			$text ="<div class='form-group'>";
			$text .="<label class='control-label'>Identificación <span class='symbol required'></span></label>";
			$text .= "<input type='text' class='form-control' id='identificacion' name='identificacion' value='".$arrVendedor->getIdentificacion()."'>";
			$text .="</div >";
			 
			$text .="<div class='form-group'>";
			$text .="<label class='control-label'>Nombre(s) <span class='symbol required'></span></label>";
			$text .="<input type='text' class='form-control' id='nombres' name='nombres' value='".$arrVendedor->getNombres()."'>";
			$text .="</div>";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Apellidos <span class='symbol required'></span> </label>";
			$text .="<input type='text'  class='form-control' id='apellidos' name='apellidos' value='".$arrVendedor->getApellidos()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Dirección<span class='symbol required'></span> </label>";
			$text .="<input type='text' class='form-control' id='direccion' name='direccion' value='".$arrVendedor->getDireccion()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Telefono<span class='symbol required'></span> </label>";
			$text .="<input type='text' class='form-control' id='telefono' name='telefono' value='".$arrVendedor->getTelefono()."'>";
			$text .="</div >";
			
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Email<span class='symbol required'></span> </label>";
			$text .="<input type='text' class='form-control' id='email' name='email' value='".$arrVendedor->getEmail()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Tipo Usuario<span class='symbol required'></span> </label>";
			$text .="<input type='text' class='form-control' id='tipoUsuario' name='tipoUsuario' value='".$arrVendedor->getTipoUsuario()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Usuario<span class='symbol required'></span> </label>";
			$text .="<input type='text' class='form-control' id='usuario' name='usuario' value='".$arrVendedor->getUsuario()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Password<span class='symbol required'></span> </label>";
			$text .="<input type='password' class='form-control' id='contrasena' name='contrasena' value='".$arrVendedor->getContrasena()."'>";
			$text .="</div >";
			
			
			
			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarRepuesto.php?respuesta=error");
		}
	}
	
	/*static public function inactivar($idVen){
		try {  
			$uni = new Empleado();
			$uni = Empleado::buscarForId($idVen);
			$uni->setEstado('Inactivo');
			$uni->actualizar();
			
			header("Location: ../Vista/BuscarVendedor.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarVendedores.php?respuesta=error&idVend=".$idVen."");
		}
	}*/
	
	/*static public function activar($idVen){
		try {  
			$uni = new Empleado();
			$uni = Empleado::buscarForId($idVen);
			$uni->setEstado('Activo');
			$uni->actualizar();
			
			header("Location: ../Vista/BuscarVendedor.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/ActivarVendedores.php?respuesta=error&idVend=".$idVen."");
		}
	}*/
	
	static public function getId ($name,$id){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$text .= "<option value=".$id.">".$id."</option>";
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
		
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$arrVendedor = ctrEmpleado::buscarForString('estado', 'Activo');
			
			$text .= "<option selected value='0'>Seleccione una opción </option>";
			foreach ($arrVendedor as $vendedor){
				$text .= "<option value=".$vendedor->getIdEmpleado().">".$vendedor->getIdEmpleado()."١ ".$vendedor->getNombres()." ".$vendedor->getApellidos()."</option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	
	}
	
	static public function rowEmpleado($idVen){
		try {
			$Vendedor = array();
			$Vendedor = ctrEmpleado::buscarID($idVen);
			
			$text = "<table class= 'table table-hover' id= 'sample-table2'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th > Identificación </th>";
			            $text .= " <th class='hidden-xs'>Nombres</th>";
			            $text .= " <th> Apellidos</th>";
						$text .= " <th class='hidden-xs'> Dirección </th>";
			            $text .= " <th >Telefono</th>";
			            $text .= " <th class='center'> Email</th>";
						$text .= " <th class='hidden-xs'>usuario</th>";
						
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
					
					$text .= "      <td class='center'>".$Vendedor->getIdEmpleado()."</td>";
					$text .= "      <td class='hidden-xs'>".$Vendedor->getIdentificacion()."</td>";
					$text .= "      <td >".$Vendedor->getNombres()."</td>";
					$text .= "      <td >".$Vendedor->getApellidos()."</td>";
					$text .= "      <td >".$Vendedor->getDireccion()."</td>";
					$text .= "      <td >".$Vendedor->getTelefono()."</td>";
					$text .= "      <td class='center'>".$Vendedor->getEmail()."</td>";
					$text .= "      <td >".$Vendedor->getUsuario()."</td>";
					
					$text .= "      <td ></td>";
					$text .= "</tr>";
					
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/rowTipoRepuesto.php?respuesta=error");
		}
	}
	
	static public function rowsEmpleado (){
		try {
			
			$arrVendedor = ctrEmpleado::buscar('tipoUsuario', 'Vendedor');
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			//var_dump($arrVendedor);
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th > Identificación </th>";
			            $text .= " <th class='hidden-xs'>Nombres</th>";
			            $text .= " <th> Apellidos</th>";
						$text .= " <th class='hidden-xs'> Dirección </th>";
			            $text .= " <th >Telefono</th>";
			            $text .= " <th class='center'> Email</th>";
						$text .= " <th class='hidden-xs'>Usuario</th>";
						
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrVendedor as $Vendedor){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$Vendedor->getIdEmpleado()."</td>";
					$text .= "      <td class='hidden-xs'>".$Vendedor->getIdentificacion()."</td>";
					$text .= "      <td >".$Vendedor->getNombres()."</td>";
					$text .= "      <td >".$Vendedor->getApellidos()."</td>";
					$text .= "      <td >".$Vendedor->getDireccion()."</td>";
					$text .= "      <td >".$Vendedor->getTelefono()."</td>";
					$text .= "      <td class='hidden-xs'>".$Vendedor->getEmail()."</td>";
					$text .= "      <td >".$Vendedor->getUsuario()."</td>";
					
					$text .= "      <td ></td>";
					
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= '../Vista/ModificarVendedor.php?idVen=".$Vendedor->getIdEmpleado()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Editar'><i class='fa fa-edit'></i></a>";
							   
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Editar";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Activar";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowConsultar($name){
		try {
			
			$arrEmpleado = ctrEmpleado::buscarForString('identificacion',$name);
			//var_dump($arrEmpleado);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			             $text .= " <th class='center'>#</th>";
			            $text .= " <th > Identificación </th>";
			            $text .= " <th class='hidden-xs'>Nombres</th>";
			            $text .= " <th> Apellidos</th>";
						$text .= " <th class='hidden-xs'> Dirección </th>";
			            $text .= " <th >Telefono</th>";
			            $text .= " <th class='center'> Email</th>";
						$text .= " <th class='hidden-xs'>usuario</th>";
						
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <tbody>";
			foreach ($arrEmpleado as $Vendedor){
			
				if ($_SESSION['cont'] == 1){
					$text .= "<tr class='success'>";
				}else if ($_SESSION['cont'] == 2){
					$text .= "<tr class='active'>";
				}else if ($_SESSION['cont'] == 3){
					$text .= "<tr class='info'>";
				}else if ($_SESSION['cont'] == 4){
					$text .= "<tr class='warning'>";
				}else if ($_SESSION['cont'] == 5){
					$text .= "<tr class='danger'>";
				}
				$text .= "      <td class='center'>".$Vendedor->getIdEmpleado()."</td>";
				$text .= "      <td class='hidden-xs'>".$Vendedor->getIdentificacion()."</td>";
				$text .= "      <td >".$Vendedor->getNombres()."</td>";
				$text .= "      <td >".$Vendedor->getApellidos()."</td>";
				$text .= "      <td >".$Vendedor->getDireccion()."</td>";
				$text .= "      <td >".$Vendedor->getTelefono()."</td>";
				$text .= "      <td class='hidden-xs'>".$Vendedor->getEmail()."</td>";
				$text .= "      <td >".$Vendedor->getUsuario()."</td>";
				
				
				$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= '../Vista/ModificarVendedor.php?idVen=".$Vendedor->getIdEmpleado()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Editar'><i class='fa fa-edit'></i></a>";
							   
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Editar";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Activar";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
				
				$text .= "</tr>";
			}
			if($_SESSION['cont'] >= 5) {
				$_SESSION['cont'] = 0;
			}		
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarTipoRepuesto.php?respuesta=error");
		}
	}
	
}

?>