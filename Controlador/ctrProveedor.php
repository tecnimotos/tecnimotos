<?php
if (!session_id())session_start();
require_once('../Modelo/Proveedor.php');

if(!empty($_GET['action'])){
	ctrProveedor::main($_GET['action']);

}

class ctrProveedor{
	
	static function main($action){
		$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrProveedor::registrar();
		}else if ($action == "buscarID"){
			ctrProveedor::buscarID('idProveedor');
		}else if ($action == "actualizar"){
			ctrProveedor::actualizar();
		}else if ($action == "inactivar"){
			ctrProveedor::inactivar($_SESSION['idPro']);
		}else if ($action == "activar"){
			ctrProveedor::activar($_SESSION['idPro']);
		}
	}
	
	static public function buscarID ($id){
		try {
			$result = Proveedor::buscarForId($id);
			return $result;
		} catch (Exception $e) {
			header("Location: ../ConsultarProveedor.php?respuesta=Correcto");
		}
	}
	
	static public function buscarAll (){
		try {
			return Proveedor::buscarAll();
		} catch (Exception $e) {
			header("");
		}
	}

	static public function buscar ($campo, $parametro){
		try {
			return Proveedor::buscar($campo, $parametro);
		} catch (Exception $e) {
			return false;
		}
	}
	
	
	
	static public function registrar (){
		try {
			$arrayProveedor = array();
			$arrayProveedor['tipoProveedor'] = $_POST['tipoProveedor'];
			$arrayProveedor['razonSocial'] = $_POST['razonSocial'];
			$arrayProveedor['nit'] = $_POST['nit'];
			$arrayProveedor['telefono'] = $_POST['telefono'];
			$arrayProveedor['ciudad'] = $_POST['ciudad'];
			$arrayProveedor['direccion'] = $_POST['direccion'];
			$arrayProveedor['email'] = $_POST['email'];
			$arrayProveedor['contacto'] = $_POST['contacto'];
			$arrayProveedor['telefonoContacto'] = $_POST['telefonoContacto'];
			$arrayProveedor['estado'] = "Activo";
			
			$Proveedor = new Proveedor ($arrayProveedor);
			$Proveedor->registrar();
			header("Location: ../Vista/BuscarProveedor.php?respuesta=correcto");
		} catch (Exception $e) {
			
			header("Location: ../Vista/RegistrarProveedor.php?respuesta=error");
		}
	}

	static public function actualizar (){
		try {
			$arrayProveedor = array();
			$arrayProveedor['tipoProveedor'] = $_POST['tipoProveedor'];
			$arrayProveedor['razonSocial'] = $_POST['razonSocial'];
			$arrayProveedor['nit'] = $_POST['nit'];
			$arrayProveedor['telefono'] = $_POST['telefono'];
			$arrayProveedor['ciudad'] = $_POST['ciudad'];
			$arrayProveedor['direccion'] = $_POST['direccion'];
			$arrayProveedor['email'] = $_POST['email'];
			$arrayProveedor['contacto'] = $_POST['contacto'];
			$arrayProveedor['telefonoContacto'] = $_POST['telefonoContacto'];
			$arrayProveedor['estado'] = $_POST['estado'];
			$arrayProveedor['idProveedor'] = $_POST['idProveedor'];
						
			$Proveedor = new Proveedor ($arrayProveedor);
			$Proveedor->actualizar();
			header("Location: ../Vista/BuscarProveedor.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarProveedor.php?respuesta=correcto");
		}
	}
	
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$arrProveedor = ctrProveedor::buscar('estado', 'Activo');
			$text .= "<option selected value='0'>Seleccione una opción</option>";
			foreach ($arrProveedor as $Proveedor){
				$text .= "<option value=".$Proveedor->getIdProveedor().">".$Proveedor->getIdProveedor()."١ ".$Proveedor->getRazonSocial()."</option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function TexboxProveedor($id){
		try {
			$arrProveedor = array();
			$arrProveedor = ctrProveedor::buscarID($id);
			
			$text ="<div class='form-group'>";
			$text .="<label class='control-label'>Tipo de Proveedor <span class='symbol required'></span></label>";
			$text .= "<input type='text' placeholder='Ingrese el marca del Repuesto' class='form-control' id='tipoProveedor' name='tipoProveedor' value='".$arrProveedor->getTipoProveedor()."'>";
			$text .="</div >";
			 
			$text .="<div class='form-group'>";
			$text .="<label class='control-label'>Razon Social <span class='symbol required'></span></label>";
			$text .="<input type='text' placeholder='Ingrese la descripcion' class='form-control' id='razonSocial' name='razonSocial' value='".$arrProveedor->getRazonSocial()."'>";
			$text .="</div>";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'> Nit <span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese la referencia' class='form-control' id='nit' name='nit' value='".$arrProveedor->getNit()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Telefono<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese el stockMinimo' class='form-control' id='telefono' name='telefono' value='".$arrProveedor->getTelefono()."'>";
			$text .="</div >";
			
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Ciudad<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese la garantia' class='form-control' id='ciudad' name='ciudad' value='".$arrProveedor->getCiudad()."'>";
			$text .="</div >";
			
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Direccion<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese el valor Base' class='form-control' id='direccion' name='direccion' value='".$arrProveedor->getDireccion()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Email<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese el stockActual ' class='form-control' id='email' name='email' value='".$arrProveedor->getEmail()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Contacto<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese la fecha de vencimiento' class='form-control' id='contacto' name='contacto' value='".$arrProveedor->getContacto()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Telefono de Contacto<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese la fecha de vencimiento' class='form-control' id='telefonoContacto' name='telefonoContacto' value='".$arrProveedor->getTelefonoContacto()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>estado<span class='symbol required'></span> </label>";
			$text .="<input type='text' placeholder='Ingrese el estado' class='form-control' id='estado' name='estado' value='".$arrProveedor->getEstado()."'>";
			$text .="</div >";
			
		
			
			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarProveedor.php?respuesta=error");
		}
	}
	
	static public function inactivar($idPro){
		try {  
			$uni = new Proveedor();
			$uni = Proveedor::buscarForId($idPro);
			$uni->setEstado('Inactivo');
			$uni->actualizar();
			
			header("Location: ../Vista/BuscarProveedor.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarProveedor.php?respuesta=error&idPro=".$idPro."");
		}
	}
	
	static public function activar($idPro){
		try {  
			$uni = new Proveedor();
			$uni = Proveedor::buscarForId($idPro);
			$uni->setEstado('Activo');
			$uni->actualizar();
			
			header("Location: ../Vista/BuscarProveedor.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarProveedor.php?respuesta=error&idPro=".$idPro."");
		}
	}
	
	static public function getUpdate ($name,$id){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$Proveedor = ctrProveedor::buscarID($id);
			
			$text .= "<option value=".$Proveedor->getIdProveedor().">".$Proveedor->getIdProveedor()."</option>";
			
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowProveedor($idPro){
		try {
			$arrProveedor = array();
			$arrProveedor = ctrProveedor::buscarID($idPro);
			//var_dump($arrRepuesto);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th>Tipo Proveedor </th>";
			            $text .= " <th class='hidden-xs'>Razon Social</th>";
			            $text .= " <th class='center'>Nit</th>";
			            $text .= " <th>Telefono</th>";
						$text .= " <th>Direccion</th>";
						$text .= " <th class='center'>Email</th>";
						$text .= " <th>Contacto</th>";
						$text .= " <th>Telefono Contacto</th>";
						
						
						
			    $text .= " </tr>";
	    	$text .= "</thead>";
			//`idRepuesto`, `marca`, `descripcion`, `referencia`, `stockMinimo`, `garantia`, `valorBase`, `tipoRepuesto`, `unidadMedida`, `estado`, `stockActual`, `fechaVencimiento`
			$text .= "  <body>";
			
					
					$text .= "      <td class='center'>".$arrProveedor->getIdProveedor()."</td>";
					$text .= "      <td class='hidden-xs'>".$arrProveedor->getTipoProveedor()."</td>";
					$text .= "      <td >".$arrProveedor->getRazonSocial()."</td>";
					$text .= "      <td> ".$arrProveedor->getNit()."</td>";
					$text .= "		<td > ".$arrProveedor->getTelefono()."</td>";
					$text .= "		<td > ".$arrProveedor->getDireccion()."</td>";
					$text .= "		<td > ".$arrProveedor->getEmail()."</td>";
					$text .= "		<td > ".$arrProveedor->getContacto()."</td>";
					$text .= "		<td > ".$arrProveedor->getTelefonoContacto()."</td>";
					
					
		//getIdRepuesto getMarca getDescripcion getReferencia getStockMinimo getGarantia getValorBase getTipoRepuesto getUnidadMedida getEstado getStockActual getFechaVencimiento 

					$text .= "</tr>";
					
			$text .= "  </body>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarProveedor.php?respuesta=error");
		}
	}
	
	
	
	
	
	static public function rowsProveedor (){
		try {
			$arrProveedor = ctrProveedor::buscarAll();
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            
			            $text .= " <th >Razon Social</th>";
			            $text .= " <th > Nit</th>";
						$text .= " <th> Telefono </th>";
			            $text .= " <th class='hidden-xs'>Ciudad</th>";
			            $text .= " <th> Direccion</th>";
			            $text .= " <th class='center'>Email</th>";
						$text .= " <th class='hidden-xs'>Contacto</th>";
						$text .= " <th >Telefono Contacto</th>";
						
			            
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrProveedor as $proveedor){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$proveedor->getIdProveedor()."</td>";
					
					$text .= "      <td >".$proveedor->getRazonSocial()."</td>";
					$text .= "      <td > ".$proveedor->getNit()."</td>";
					$text .= "      <td class='center'>".$proveedor->getTelefono()."</td>";
					$text .= "      <td class='hidden-xs'>".$proveedor->getCiudad()."</td>";
					$text .= "      <td >".$proveedor->getDireccion()."</td>";
					$text .= "      <td> ".$proveedor->getEmail()."</td>";
					$text .= "      <td >".$proveedor->getContacto()."</td>";
					$text .= "      <td >".$proveedor->getTelefonoContacto()."</td>";	
							
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= '../Vista/ModificarProveedor.php?idPro=".$proveedor->getIdProveedor()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Actualizar'><i class='fa fa-edit'></i></a>";
							    $text .= " <a href= '../Vista/ActivarProveedor.php?idPro=".$proveedor->getIdProveedor()."' class= 'btn btn-xs btn-green tooltips' data-placement= 'top' data-original-title= 'Activar'><i class='fa fa-share'></i></a>";
							    $text .= " <a href= '../Vista/InactivarProveedor.php?idPro=".$proveedor->getIdProveedor()."' class= 'btn btn-xs btn-red tooltips' data-placement= 'top' data-original-title= 'Inactivar'><i class='fa fa-times fa fa-white'></i></a>";
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Activar";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowConsultar($name){
		try {
			
			$arrRazonSocial = ctrProveedor::buscar('razonSocial',$name);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th class='hidden-xs'>Razon Social</th>";
			            $text .= " <th class='center'> Nit</th>";
						$text .= " <th> Telefono </th>";
			            $text .= " <th class='hidden-xs'>Ciudad</th>";
			            $text .= " <th> Direccion</th>";
			            $text .= " <th class='center'>Email</th>";
						$text .= " <th class='hidden-xs'>Contacto</th>";
						$text .= " <th class='center'>Telefono de Contacto</th>";
						;
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <body>";
			foreach ($arrRazonSocial as $proveedor){
			
				if ($_SESSION['cont'] == 1){
					$text .= "<tr class='success'>";
				}else if ($_SESSION['cont'] == 2){
					$text .= "<tr class='active'>";
				}else if ($_SESSION['cont'] == 3){
					$text .= "<tr class='info'>";
				}else if ($_SESSION['cont'] == 4){
					$text .= "<tr class='warning'>";
				}else if ($_SESSION['cont'] == 5){
					$text .= "<tr class='danger'>";
				}
				
				$text .= "      <td class='center'>".$proveedor->getIdProveedor()."</td>";
				$text .= "      <td >".$proveedor->getRazonSocial()."</td>";
				$text .= "      <td > ".$proveedor->getNit()."</td>";
				$text .= "      <td class='center'>".$proveedor->getTelefono()."</td>";
				$text .= "      <td class='hidden-xs'>".$proveedor->getCiudad()."</td>";
				$text .= "      <td >".$proveedor->getDireccion()."</td>";
				$text .= "      <td> ".$proveedor->getEmail()."</td>";
				$text .= "      <td >".$proveedor->getContacto()."</td>";
				$text .= "      <td >".$proveedor->getTelefonoContacto()."</td>";	
				
				$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= '../Vista/ModificarProveedor.php?idPro=".$proveedor->getIdProveedor()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Actualizar'><i class='fa fa-edit'></i></a>";
							    $text .= " <a href= '../Vista/ActivarProveedor.php?idPro=".$proveedor->getIdProveedor()."' class= 'btn btn-xs btn-green tooltips' data-placement= 'top' data-original-title= 'Activar'><i class='fa fa-share'></i></a>";
							    $text .= " <a href= '../Vista/InactivarProveedor.php?idPro=".$proveedor->getIdProveedor()."' class= 'btn btn-xs btn-red tooltips' data-placement= 'top' data-original-title= 'Inactivar'><i class='fa fa-times fa fa-white'></i></a>";
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Activar";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
				$text .= "</tr>";
			}
			if($_SESSION['cont'] >= 5) {
				$_SESSION['cont'] = 0;
			}		
			$text .= "  </body>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarTipoRepuesto.php?respuesta=error");
		}
	}
	
}
?>