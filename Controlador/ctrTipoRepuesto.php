<?php

require_once('../Modelo/TipoRepuesto.php');
if (!session_id())session_start();
if(!empty($_GET['action'])){
	ctrTipoRepuesto::main($_GET['action']);

}

class ctrTipoRepuesto{
	
	static function main($action){
		$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrTipoRepuesto::registrar();
		}else if ($action == "buscarID"){
			ctrTipoRepuesto::buscarID($_POST['idTipoRepuesto']);
		}else if ($action == "actualizar"){
			ctrTipoRepuesto::actualizar();
		}else if ($action == "inactivar"){
			ctrTipoRepuesto::inactivar($_SESSION['idTipoR']);
		}else if ($action == "activar"){
			ctrTipoRepuesto::activar($_SESSION['idTipoR']);
		}
	}
	
	static public function buscarID ($id){
		try {
			return TipoRepuesto::buscarForId($id);
		} catch (Exception $e) {
			header("Location: ../Vista/registrarTipoRepuesto.php?respuesta=error");
		}
	}
	
	static public function buscarAll (){
		try {
			return TipoRepuesto::buscarAll();
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarTipoRepuesto.php?respuesta=error");
		}
	}

	static public function buscar ($campo, $parametro){
		try {
			return TipoRepuesto::buscar($campo, $parametro);
		} catch (Exception $e) {
			return false;
		}
	}
	
	/*static public function buscarForString ($campo, $parametro){
		try {
			return TipoRepuesto::buscarForString($campo, $parametro);
		} catch (Exception $e) {
			return false;
		}
	}*/
	
	static public function TexboxTipoRepuesto($id){
		try {
			$arrUnidadMedida = array();
			$arrUnidadMedida = ctrTipoRepuesto::buscarID($id);
			
			$text ="<div class='form-group'>";
			$text .="<label class='control-label'>Nombre <span class='symbol required'></span></label>";
			$text .= "<input type='text' placeholder='Ingrese el Nombre de Tipo de Repuesto' class='form-control' id='nombre' name='nombre' value='".$arrUnidadMedida->getNombre()."'>";
			$text .="</div >";
			 
			$text .="<div class='form-group'>";
			$text .="<label class='control-label'>Descripcion <span class='symbol required'></span></label>";
			$text .="<input type='text' placeholder='Ingrese la descripcion' class='form-control' id='descripcion' name='descripcion' value='".$arrUnidadMedida->getDescripcion()."'>";
			$text .="</div>";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'> Estado <span class='symbol required'></span> </label>";
			$text .="<input type=type='text' placeholder='Ingrese el estado' class='form-control' id='estado' name='estado' value='".$arrUnidadMedida->getEstado()."'>";
			$text .="</div >";

			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarTipoRepuesto.php?respuesta=error");
		}
	}
	
	static public function inactivar($idTipoR){
		try {  
			$uni = new ctrTipoRepuesto();
			$uni = ctrTipoRepuesto::buscarID($idTipoR);
			$uni->setEstado('Inactivo');
			$uni->actualizar();
			
			header("Location: ../Vista/BuscarTipoRepuesto.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarTipoRepuesto.php?respuesta=error&idTipoR=".$idTipoR."");
		}
	}
	
	static public function activar($idTipoR){
		try {  
			$uni = new ctrTipoRepuesto();
			$uni = ctrTipoRepuesto::buscarID($idTipoR);
			$uni->setEstado('Activo');
			$uni->actualizar();
			
			header("Location: ../Vista/BuscarTipoRepuesto.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/ActivarTipoRepuesto.php?respuesta=error&idTipoR=".$idTipoR."");
		}
	}
	
	static public function registrar (){
		try {
			$arrayTipoRepuesto = array();
			$arrayTipoRepuesto['nombre'] = $_POST['nombre'];
			$arrayTipoRepuesto['descripcion'] = $_POST['descripcion'];
			$arrayTipoRepuesto['estado'] = "Activo";
			$tipoRepuesto = new TipoRepuesto ($arrayTipoRepuesto);
			$tipoRepuesto->registrar();
			header("Location: ../Vista/BuscarTipoRepuesto.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/RegistrarTipoRepuesto.php?respuesta=error");
		}
	}

	static public function actualizar (){
		try {
			$arrayTipoRepuesto = array();
			$arrayTipoRepuesto['nombre'] = $_POST['nombre'];
			$arrayTipoRepuesto['descripcion'] = $_POST['descripcion'];
			$arrayTipoRepuesto['estado'] = $_POST['estado'];
			$arrayTipoRepuesto['idTipoRepuesto'] = $_POST['idTipoRepuesto'];
			
			$TipoRepuesto = new TipoRepuesto ($arrayTipoRepuesto);
			$TipoRepuesto->actualizar();
			header("Location: ../Vista/BuscarTipoRepuesto.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/ModificarTipoRepuesto.php?respuesta=error");
		}
	}
	
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$arrTipoRepuesto = ctrTipoRepuesto::buscar('estado', 'Activo');
			$text .= "<option selected value='0'>Seleccione una opción</option>";
			foreach ($arrTipoRepuesto as $TipoRepuesto){
				$text .= "<option value=".$TipoRepuesto->getIdTipoRepuesto().">".$TipoRepuesto->getIdTipoRepuesto()."١ ".$TipoRepuesto->getNombre()."</option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function getUpdate ($name,$id){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$text .= "<option value=".$id.">".$id."</option>";
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowTipoRepuesto($idTipoR){
		try {
			$arrUnidadMedida = array();
			$arrUnidadMedida = ctrTipoRepuesto::buscarID($idTipoR);
			
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Nombre </th>";
			            $text .= " <th class='hidden-xs'>Descripción</th>";
			           
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
				
					$text .= "      <td class='center'>".$arrUnidadMedida->getIdTipoRepuesto()."</td>";
					$text .= "      <td class='hidden-xs'>".$arrUnidadMedida->getNombre()."</td>";
					$text .= "      <td >".$arrUnidadMedida->getDescripcion()."</td>";
					
					$text .= "		<td class='center'> ";

					$text .= "		</td>";
					$text .= "</tr>";
				
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarTipoRepuesto.php?respuesta=error");
		}
	}
	
	static public function rowsTipoRepuesto (){
		try {
			$arrTipoRepuesto = ctrTipoRepuesto::buscarAll();
			$text = "<table class= 'table table-hover' id= 'sampleTable2'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th > Nombre </th>";
			            $text .= " <th >Descripción</th>";
			            $text .= " <th class='center'>Estado</th>";
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrTipoRepuesto as $tiporepuesto){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$tiporepuesto->getIdTipoRepuesto()."</td>";
					$text .= "      <td class='hidden-xs'>".$tiporepuesto->getNombre()."</td>";
					$text .= "      <td >".$tiporepuesto->getDescripcion()."</td>";
					$text .= "      <td class='center'> ".$tiporepuesto->getEstado()."</td>";
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= 'ModificarTipoRepuesto.php?idTipoR=".$tiporepuesto->getIdTipoRepuesto()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Editar'><i class='fa fa-edit'></i></a>";
							    $text .= " <a href= ' ../Vista/ActivarTipoRepuesto.php?idTipoR=".$tiporepuesto->getIdTipoRepuesto()."' class= 'btn btn-xs btn-green tooltips' data-placement= 'top' data-original-title= 'Activar'><i class='fa fa-share'></i></a>";
							    $text .= " <a href= ' ../Vista/InactivarTipoRepuesto.php?idTipoR=".$tiporepuesto->getIdTipoRepuesto()."' class= 'btn btn-xs btn-red tooltips' data-placement= 'top' data-original-title= 'Inactivar'><i class='fa fa-times fa fa-white'></i></a>";
					    $text .= "</div>";
						
					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Share";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	
		
	static public function rowConsultar($name){
		try {
			
			$arrTipoRepuesto = ctrTipoRepuesto::buscar('nombre',$name);
			//echo $_SESSION['cont'];
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Nombre </th>";
			            $text .= " <th>Descripción</th>";
			            $text .= " <th >Estado</th>";
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <tbody>";
			foreach ($arrTipoRepuesto as $tiporepuesto){
				
					if ($_SESSION['cont'] == 1){
						$text .= "<tr class='success'>";
					}else if ($_SESSION['cont'] == 2){
						$text .= "<tr class='active'>";
					}else if ($_SESSION['cont'] == 3){
						$text .= "<tr class='info'>";
					}else if ($_SESSION['cont'] == 4){
						$text .= "<tr class='warning'>";
					}else if ($_SESSION['cont'] == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$tiporepuesto->getIdTipoRepuesto()."</td>";
					$text .= "      <td >".$tiporepuesto->getNombre()."</td>";
					$text .= "      <td >".$tiporepuesto->getDescripcion()."</td>";
					$text .= "      <td> ".$tiporepuesto->getEstado()."</td>";
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= 'ModificarTipoRepuesto.php?idTipoR=".$tiporepuesto->getIdTipoRepuesto()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Editar'><i class='fa fa-edit'></i></a>";
							    $text .= " <a href= ' ../Vista/ActivarTipoRepuesto.php?idTipoR=".$tiporepuesto->getIdTipoRepuesto()."' class= 'btn btn-xs btn-green tooltips' data-placement= 'top' data-original-title= 'Activar'><i class='fa fa-share'></i></a>";
							    $text .= " <a href= ' ../Vista/InactivarTipoRepuesto.php?idTipoR=".$tiporepuesto->getIdTipoRepuesto()."' class= 'btn btn-xs btn-red tooltips' data-placement= 'top' data-original-title= 'Inactivar'><i class='fa fa-times fa fa-white'></i></a>";
					    $text .= "</div>";
						
					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Share";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
			}
			if($_SESSION['cont'] >= 5) {
				$_SESSION['cont'] = 0;
			}		
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarTipoRepuesto.php?respuesta=error");
		}
	}
}
?>