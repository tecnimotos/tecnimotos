<?php

require_once('../Modelo/Venta.php');
//require_once('../Controlador/ctrDescripcionVenta.php');

if (!session_id())session_start();

if(!empty($_GET['action'])){
	ctrVenta::main($_GET['action']);
	
}

	class ctrVenta{
	
	static function main($action){
		$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrVenta::registrar();
		}else if ($action == "buscarID"){
			ctrVenta::buscarID($_POST['idVenta']);
		}else if ($action == "actualizar"){
			ctrVenta::actualizar();
		}
	}
	static public function buscarID ($id){
		try {
			return Venta::buscarForId($id);
		} catch (Exception $e) {
			header("Location: ../Index.php?respuesta=correcto");
		}
	}
	
	static public function buscarAll (){
		try {
			return Venta::buscarAll();
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function buscar ($campo, $parametro){
		try {
			return Venta::buscar($campo, $parametro);
		} catch (Exception $e) {
			header("");
		}
	}
	static public function buscarForString ($campo, $parametro){
		try {
			return Venta::buscarForString($campo, $parametro);
		} catch (Exception $e) {
			return false;
		}
	}

    static public function inactivar($id){
	
	}
	
	static public function registrar (){
		try {
			$arrayVenta = array();
			$arrayVenta['fechaVenta'] =date("Y-m-d");
			$arrayVenta['referencia'] = $_SESSION['referenciaVenta'] = $_POST['referencia'];
			$arrayVenta['vendedor'] = Persona::buscarForId($_POST['idEmpleado']);
			$arrayVenta['cliente'] = Persona::buscarForId($_POST['idPersona']);
			$arrayVenta['valorTotal'] = $_POST['valorTotal'];
			
			$Venta = new Venta ($arrayVenta);
			$Venta->registrar();
			$_SESSION['idVenta'] = $Venta->getIdVenta();
			header("Location: ../Vista/RegistrarVenta.php?respuesta=correcto");
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$arrVenta = ctrVenta::buscarAll();
			$text .= "<option selected value='0'>Seleccione una opción</option>";
			foreach ($arrVenta as $Venta){
				$text .= "<option value=".$Venta->getIdVenta().">".$Venta->getReferencia()."</option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	/*static public function rowsDescripcionVenta (){
		try {
			$arrDescripcionVenta = ctrDescripcionVenta::buscarAll ();
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th class='center'> Cantidad </th>";
			            $text .= " <th >Valor Unitario</th>";
			            $text .= " <th> Descripcion</th>";
			            $text .= " <th >Valor de Producto</th>";
			            $text .= " <th >Venta</th>";
						$text .= " <th class='center'>Repuesto</th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
				foreach ($arrDescripcionVenta as $desventa){
					
					$text .= "<tr>";
					$text .= "      <td class='center'>".$desventa->getIdDescripcionVenta()."</td>";
					$text .= "      <td class='center'>".$desventa->getCantidad()."</td>";
					$text .= "      <td > ".$desventa->getValorUnitario()."</td>";
					$text .= "      <td > ".$desventa->getDescripcion()."</td>";
					$text .= "      <td > ".$desventa->getValorProducto()."</td>";
					$text .= "      <td > ".$desventa->getVenta()->getReferencia()."</td>";
					$text .= "      <td class='center'> ".$desventa->getRepuesto()->getReferencia()."</td>";
					$text .= "		<td class='center'> ";

						
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Add";
									    $text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}*/
	
	static public function rowsVenta (){
		try {
			$arrVenta = ctrVenta::buscarAll ();
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Fecha de Venta </th>";
			            $text .= " <th> Referencia</th>";
			            $text .= " <th class='hidden-xs'>Vendedor</th>";
			            $text .= " <th>Cliente</th>";
						$text .= " <th class='hidden-xs'>Valor Total</th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrVenta as $venta){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$venta->getIdVenta()."</td>";
					$text .= "      <td class='hidden-xs'>".$venta->getFechaVenta()."</td>";
					$text .= "      <td> ".$venta->getReferencia()."</td>";
					$text .= "      <td> ".$venta->getVendedor()->getNombres()." ".$venta->getVendedor()->getApellidos()."</td>";
					$text .= "      <td> ".$venta->getCliente()->getNombres()." ".$venta->getCliente()->getApellidos()."</td>";
					$text .= "      <td >".$venta->getValorTotal()."</td>";
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							  
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								 $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function getUpdate ($name,$id){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$Venta = ctrVenta::buscarID($id);
			$_SESSION['datosVenta'] = $Venta;
			
			$text .= "<option value=".$Venta->getIdVenta()."> ".$Venta->getReferencia()."</option>";
			
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	static public function rowwVenta($id){
		try {
			
			$arrVenta = ctrVenta::buscar('idVenta',$id);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th>fechaVenta</th>";
			            $text .= " <th class='hidden-xs'>Referencia</th>";
					    $text .= " <th class='hidden-xs'>Vendedor</th>";
			            $text .= " <th class='hidden-xs'>Cliente</th>";
						$text .= " <th class='hidden-xs'>Valor Total</th>";
						
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <tbody>";
			foreach ($arrVenta as $Venta){
			
					if ($_SESSION['cont'] == 1){
						$text .= "<tr class='success'>";
					}else if ($_SESSION['cont'] == 2){
						$text .= "<tr class='active'>";
					}else if ($_SESSION['cont'] == 3){
						$text .= "<tr class='info'>";
					}else if ($_SESSION['cont'] == 4){
						$text .= "<tr class='warning'>";
					}else if ($_SESSION['cont'] == 5){
						$text .= "<tr class='danger'>";
					}
					$text .= "      <td class='center'>".$Venta->getIdVenta()."</td>";
					$text .= "      <td class='center'>".$Venta->getFechaVenta()."</td>";
					$text .= "      <td >".$Venta->getReferencia()."</td>";
					$text .= "      <td class='hidden-xs'>".$Venta->getVendedor()->getNombres()."</td>";
					$text .= "      <td class='hidden-xs'>".$Venta->getCliente()->getNombres()."</td>";
					$text .= "      <td class='hidden-xs'>".$Venta->getValorTotal()."</td>";
					
				
					$text .= "		<td class='center'> ";

					$text .= "		</td>";
					$text .= "</tr>";
			}
			if($_SESSION['cont'] >= 5) {
				$_SESSION['cont'] = 0;
			}		
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarVenta.php?respuesta=error");
		}
	}
}
?>