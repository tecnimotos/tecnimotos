<?php

require_once('../Modelo/UnidadMedida.php');
if (!session_id())session_start();

if(!empty($_GET['action'])){
	ctrUnidadMedida::main($_GET['action']);

}

class ctrUnidadMedida{
	
	static function main($action){
		$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrUnidadMedida::registrar();
		}else if ($action == "buscarID"){
			ctrUnidadMedida::buscarID($_POST['idUnidadMedida']);
		}else if ($action == "actualizar"){
			ctrUnidadMedida::actualizar();
		}else if ($action == "inactivar"){
			ctrUnidadMedida::inactivar($_SESSION['idUnidad']);
		}else if ($action == "activar"){
			ctrUnidadMedida::activar($_SESSION['idUnidad']);
		}
	}
	
	static public function buscarID ($id){
		try {
			return UnidadMedida::buscarForId($id);
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function buscarAll (){
		try {
			return UnidadMedida::buscarAll();
		} catch (Exception $e) {
			header("");
		}
	}

	static public function buscar ($campo, $parametro){
		try {
			return UnidadMedida::buscar($campo, $parametro);
		} catch (Exception $e) {
			return false;
		}
	}
	
	static public function inactivar($idUnidad){
		try {  
			$uni = new ctrUnidadMedida();
			$uni = ctrUnidadMedida::buscarID($idUnidad);
			$uni->setEstado('Inactivo');
			$uni->actualizar();
			if ($action == "consultar"){
				header("Location: ../Vista/ConsultarUnidadMedida.php?respuesta=correcto");
			}else {
				header("Location: ../Vista/BuscarUnidadMedida.php?respuesta=correcto");
			}
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarUnidadMedida.php?respuesta=error");
		}
	}
	
	static public function activar($idUnidad){
		try {  
			$uni = new ctrUnidadMedida();
			$uni = ctrUnidadMedida::buscarID($idUnidad);
			$uni->setEstado('Activo');
			$uni->actualizar();
			
			header("Location: ../Vista/BuscarUnidadMedida.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarUnidadMedida.php?respuesta=error");
		}
	}
	
	static public function registrar (){
		try {
			$arrayUnidadMedida = array();
			$arrayUnidadMedida['nombre'] = $_POST['nombre'];
			$arrayUnidadMedida['descripcion'] = $_POST['descripcion'];
			$arrayUnidadMedida['tipo'] = $_POST['tipo'];
			$arrayUnidadMedida['estado'] = "Activo";
			
			$UnidadMedida = new UnidadMedida ($arrayUnidadMedida);
			$UnidadMedida->registrar();
			header("Location: ../Vista/BuscarUnidadMedida.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/RegistrarUnidadMedida.php?respuesta=error");
		}
	}

	static public function actualizar (){
		try {
			$arrayUnidadMedida = array();
			$arrayUnidadMedida['nombre'] = $_POST['nombre'];
			$arrayUnidadMedida['descripcion'] = $_POST['descripcion'];
			$arrayUnidadMedida['tipo'] = $_POST['tipo'];
			$arrayUnidadMedida['estado'] = $_POST['estado'];
			$arrayUnidadMedida['idUnidadMedida'] = $_POST['idUnidadMedida'];
			
			$UnidadMedida = new UnidadMedida ($arrayUnidadMedida);
			$UnidadMedida->actualizar();
			header("Location: ../Vista/BuscarUnidadMedida.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/ModificarUnidadMedida.php?respuesta=error");
		}
	}
	
	static public function TexboxUnidadMedida($id){
		try {
			$arrUnidadMedida = array();
			$arrUnidadMedida = ctrUnidadMedida::buscarID($id);
			
			$text ="<div class='form-group'>";
			$text .="<label class='control-label'>Nombre <span class='symbol required'></span></label>";
			$text .= "<input type='text' placeholder='Ingrese el Nombre de Tipo de Repuesto' class='form-control' id='nombre' name='nombre' value='".$arrUnidadMedida->getNombre()."'>";
			$text .="</div >";
			 
			$text .="<div class='form-group'>";
			$text .="<label class='control-label'>Descripcion <span class='symbol required'></span></label>";
			$text .="<input type='text' placeholder='Ingrese la descripcion' class='form-control' id='descripcion' name='descripcion' value='".$arrUnidadMedida->getDescripcion()."'>";
			$text .="</div>";
			
			$text .="<div class='form-group'>";
			$text .="<label class='control-label'>Tipo <span class='symbol required'></span></label>";
			$text .="<input type='text' placeholder='Ingrese la descripcion' class='form-control' id='tipo' name='tipo' value='".$arrUnidadMedida->getTipo()."'>";
			$text .="</div>";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'> Estado <span class='symbol required'></span> </label>";
			$text .="<input type=type='text' placeholder='Ingrese el estado' class='form-control' id='estado' name='estado' value='".$arrUnidadMedida->getEstado()."'>";
			$text .="</div >";

			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarTipoRepuesto.php?respuesta=error");
		}
	}
	
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestraunidadmedida(this.value)";
			$arrUnidadMedida = ctrUnidadMedida::buscar('estado', 'Activo');
			$text .= "<option selected value='0'>Seleccione una opción</option>";
			foreach ($arrUnidadMedida as $UnidadMedida){
				$text .= "<option value=".$UnidadMedida->getIdUnidadMedida()."> ".$UnidadMedida->getIdUnidadMedida()."١ ".$UnidadMedida->getNombre()."</option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function getUpdate ($name,$id){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$text .= "<option value=".$id.">".$id."</option>";
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowUnidadMedida ($idUnidad){
		try {
			$arrUnidadMedida = array();
			$arrUnidadMedida = ctrUnidadMedida::buscarID ($idUnidad);
			//echo $_SESSION['cont'];
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Nombre </th>";
			            $text .= " <th class='hidden-xs'>Descripcion</th>";
			            $text .= " <th> Tipo</th>";
			           
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <body>";
			
				
					
					$text .= "      <td class='center'>".$arrUnidadMedida->getIdUnidadMedida()."</td>";
					$text .= "      <td class='hidden-xs'>".$arrUnidadMedida->getNombre()."</td>";
					$text .= "      <td >".$arrUnidadMedida->getDescripcion()."</td>";
					$text .= "      <td> <a href= '#'' rel= 'nofollow' target= '_blank'>".$arrUnidadMedida->getTipo()."</td>";
					
					
					$text .= "</tr>";
				
			$text .= "  </body>";
			$text .= "</table>";	
			return $text;
			header("Location: ../Vista/InactivarUnidadMedida.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarUnidadMedida.php?respuesta=error");
		}
	}
	
	static public function rowsUnidadMedida (){
		try {
			$arrUnidadMedida = ctrUnidadMedida::buscarAll ();
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Nombre </th>";
			            $text .= " <th class='hidden-xs'>Descripcion</th>";
			            $text .= " <th> Tipo</th>";
			            $text .= " <th class='hidden-xs'>Estado</th>";
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <body>";
			
			$cont = 0;
			
				foreach ($arrUnidadMedida as $unidad){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$unidad->getIdUnidadMedida()."</td>";
					$text .= "      <td class='hidden-xs'>".$unidad->getNombre()."</td>";
					$text .= "      <td >".$unidad->getDescripcion()."</td>";
					$text .= "      <td> ".$unidad->getTipo()."</td>";
					$text .= "      <td> ".$unidad->getEstado()."</td>";
					$text .= "		<td class='center'> ";
					
						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= 'ModificarUnidadMedida.php?idUnidad=".$unidad->getIdUnidadMedida()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Edit'><i class='fa fa-edit'></i></a>";
							    $text .= " <a href= ' ../Vista/ActivarUnidadMedida.php?idUnidad=".$unidad->getIdUnidadMedida()."' class= 'btn btn-xs btn-green tooltips' data-placement= 'top' data-original-title= 'Activar'><i class='fa fa-share'></i></a>";
							    $text .= " <a href= ' ../Vista/InactivarUnidadMedida.php?idUnidad=".$unidad->getIdUnidadMedida()."' class= 'btn btn-xs btn-red tooltips' data-placement= 'top' data-original-title= 'Inactivar'><i class='fa fa-times fa fa-white' ></i></a > ";
					    $text .= "</div>";
						//ctrUnidadMedida::inactivar(idUnidad);
					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Share";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </body>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowConsultar($name){
		try {
			
			$arrUnidadMedida = ctrUnidadMedida::buscar('nombre',$name);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Nombre </th>";
			            $text .= " <th class='hidden-xs'>Descripción</th>";
						$text .= " <th class='hidden-xs'>Tipo</th>";
			            $text .= " <th class='hidden-xs'>Estado</th>";
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <body>";
			if($_SESSION['cont'] > 5) {
				$_SESSION['cont'] = 1;
			}
					
				foreach ($arrUnidadMedida as $unidad){
					
					if ($_SESSION['cont'] == 1){
						$text .= "<tr class='success'>";
					}else if ($_SESSION['cont'] == 2){
						$text .= "<tr class='active'>";
					}else if ($_SESSION['cont'] == 3){
						$text .= "<tr class='info'>";
					}else if ($_SESSION['cont'] == 4){
						$text .= "<tr class='warning'>";
					}else if ($_SESSION['cont'] == 5){
						$text .= "<tr class='danger'>";
					}
					$text .= "      <td class='center'>".$unidad->getIdUnidadMedida()."</td>";
					$text .= "      <td class='hidden-xs'>".$unidad->getNombre()."</td>";
					$text .= "      <td >".$unidad->getDescripcion()."</td>";
					$text .= "      <td> ".$unidad->getTipo()."</td>";
					$text .= "      <td> ".$unidad->getEstado()."</td>";
					$text .= "		<td class='center'> ";
					
						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= 'ModificarUnidadMedida.php?idUnidad=".$unidad->getIdUnidadMedida()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Edit'><i class='fa fa-edit'></i></a>";
							    $text .= " <a href= ' ../Vista/ActivarUnidadMedida.php?idUnidad=".$unidad->getIdUnidadMedida()."' class= 'btn btn-xs btn-green tooltips' data-placement= 'top' data-original-title= 'Activar'><i class='fa fa-share'></i></a>";
							    $text .= " <a href= ' ../Vista/InactivarUnidadMedida.php?idUnidad=".$unidad->getIdUnidadMedida()."' class= 'btn btn-xs btn-red tooltips' data-placement= 'top' data-original-title= 'Inactivar'><i class='fa fa-times fa fa-white' ></i></a > ";
					    $text .= "</div>";
						//ctrUnidadMedida::inactivar(idUnidad);
					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Share";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					
				}
				
			$text .= "  </body>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarUnidadMedida.php?respuesta=error");
		}
	}

	/*static public function buscarForString ($campo, $parametro){
		try {
			return UnidadMedida::buscarForString($campo, $parametro);
		} catch (Exception $e) {
			return false;
		}
	}*/
	
}
?>