<?php

require_once('../Modelo/Persona.php');

if (!session_id())session_start();

if(!empty($_GET['action'])){
	ctrPersona::main($_GET['action']);
	
}
class ctrPersona{
	
	static function main($action){
		$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrPersona::registrar();
	    }else if ($action == "BuscarAll"){
			ctrPersona::buscarAll($_POST['BuscarAll']);
		}else if ($action == "Buscar"){
			ctrPersona::buscar($_POST['Buscar']);
		}else if ($action == "Inactivar"){
			ctrPersona::inactivar($_POST['Inactivar']);
			
		}
	}
	
	static public function registrar (){
	try {
			$arrayPersonas = array();
			$arrayPersonas['tipoUsuario'] = $_POST['tipoUsuario'];
			$arrayPersonas['identificacion'] = $_POST['identificacion'];
			$arrayPersonas['nombres'] = $_POST['nombres'];
			$arrayPersonas['apellidos'] = $_POST['apellidos'];
			$arrayPersonas['direccion'] = $_POST['direccion'];
			$arrayPersonas['telefono'] = $_POST['telefono'];
			$arrayPersonas['email'] = $_POST['email'];
			
			
			$Persona = new Persona ($arrayPersonas);
			$Persona->registrar();
		header("Location: ../Vista/Index.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/RegistrarPersona.php?respuesta=error");
	}
	}
	static public function actualizar (){
		try {
			
			
			$arrayPersonas = array();
			$arrayPersonas['tipoUsuario'] = $_POST['tipoUsuario'];
			$arrayPersonas['identificacion'] = $_POST['identificacion'];
			$arrayPersonas['nombre'] = $_POST['nombre'];
			$arrayPersonas['apellido'] = $_POST['apellido'];
			$arrayPersonas['direccion'] = $_POST['direccion'];
			$arrayPersonas['telefono'] = $_POST['telefono'];
			$arrayPersonas['email'] = $_POST['email'];
			$arrayPersonas['idPersona'] = $_POST['idPersona'];
			
			
			$Persona = new Persona ($arrayPersonas);
			$Persona->actualizar();
			header("");
		} catch (Exception $e) {
			header("");
		}
	}
		static public function buscarID ($id){
		try {
			return Persona::buscarForId($id);
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function buscarAll (){
		//try {
			return Persona::buscarAll();
		//} catch (Exception $e) {
			header("");
		//}
	}
	
	static public function buscarForString ($campo, $parametro){
		try {
			return Persona::buscarForString($campo, $parametro);
		} catch (Exception $e) {
			return false;
		}
	}

	static public function buscar ($campo, $parametro){
		try {
			return Persona::buscar($campo, $parametro);
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>";
			$arrPersonas = ctrPersona::buscar("tipoUsuario", array("Cliente"));
			$text .= "<option selected value='0'>Seleccione una opción</option>";
			foreach ($arrPersonas as $persona){
				$text .= "<option value=".$persona->getIdPersona()."> ".$persona->getIdPersona()."١ ".$persona->getNombres()." ".$persona->getApellidos()."</option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowsCliente (){
		try {
			$arrCliente = ctrPersona::buscar("tipoUsuario", array("Cliente"));
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Identificación </th>";
			            $text .= " <th class='hidden-xs'>Nombres</th>";
			            $text .= " <th> Apellidos</th>";
						$text .= " <th> Direccion </th>";
			            $text .= " <th class='hidden-xs'>Telefono</th>";
			            $text .= " <th class='center'> Email</th>";
			           
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrCliente as $cliente){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$cliente->getIdPersona()."</td>";
					$text .= "      <td class='hidden-xs'>".$cliente->getIdentificacion()."</td>";
					$text .= "      <td >".$cliente->getNombres()."</td>";
					$text .= "      <td> ".$cliente->getApellidos()."</td>";
					$text .= "      <td >".$cliente->getDireccion()."</td>";
					$text .= "      <td class='hidden-xs'>".$cliente->getTelefono()."</td>";
					$text .= "      <td >".$cliente->getEmail()."</td>";
					
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowsEmpleado ($name){
		try {
			$arrCliente = ctrPersona::buscar("tipoUsuario", array("Vendedor"));
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Identificación </th>";
			            $text .= " <th class='hidden-xs'>Nombres</th>";
			            $text .= " <th> Apellidos</th>";
						$text .= " <th> Direccion </th>";
			            $text .= " <th class='hidden-xs'>Telefono</th>";
			            $text .= " <th> Email</th>";
			           
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrCliente as $cliente){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$cliente->getIdPersona()."</td>";
					$text .= "      <td class='hidden-xs'>".$cliente->getIdentificacion()."</td>";
					$text .= "      <td >".$cliente->getNombres()."</td>";
					$text .= "      <td> <a href= '#'' rel= 'nofollow' target= '_blank'>".$cliente->getApellidos()."</td>";
					$text .= "      <td class='center'>".$cliente->getDireccion()."</td>";
					$text .= "      <td class='hidden-xs'>".$cliente->getTelefono()."</td>";
					$text .= "      <td >".$cliente->getEmail()."</td>";
					
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= '#' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Edit'><i class='fa fa-edit'></i></a>";
							    $text .= " <a href= '#' class= 'btn btn-xs btn-green tooltips' data-placement= 'top' data-original-title= 'Share'><i class='fa fa-share'></i></a>";
							    $text .= " <a href= '#' class= 'btn btn-xs btn-red tooltips' data-placement= 'top' data-original-title= 'Inactivar'><i class='fa fa-times fa fa-white'></i></a>";
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Share";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowConsultar($name){
		try {
			
			$arrCliente = ctrPersona::buscarForString('identificacion',$name);
			//var_dump($arrEmpleado);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			             $text .= " <th class='center'>#</th>";
			            $text .= " <th > Identificación </th>";
			            $text .= " <th class='hidden-xs'>Nombres</th>";
			            $text .= " <th> Apellidos</th>";
						$text .= " <th class='hidden-xs'> Dirección </th>";
			            $text .= " <th >Telefono</th>";
			            $text .= " <th class='center'> Email</th>";
						
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <tbody>";
			foreach ($arrCliente as $cliente){
				
				if ($_SESSION['cont'] == 1){
					$text .= "<tr class='success'>";
				}else if ($_SESSION['cont'] == 2){
					$text .= "<tr class='active'>";
				}else if ($_SESSION['cont'] == 3){
					$text .= "<tr class='info'>";
				}else if ($_SESSION['cont'] == 4){
					$text .= "<tr class='warning'>";
				}else if ($_SESSION['cont'] == 5){
					$text .= "<tr class='danger'>";
				}
				
				$text .= "      <td class='center'>".$cliente->getIdPersona()."</td>";
				$text .= "      <td class='hidden-xs'>".$cliente->getIdentificacion()."</td>";
				$text .= "      <td >".$cliente->getNombres()."</td>";
				$text .= "      <td >".$cliente->getApellidos()."</td>";
				$text .= "      <td >".$cliente->getDireccion()."</td>";
				$text .= "      <td >".$cliente->getTelefono()."</td>";
				$text .= "      <td class='hidden-xs'>".$cliente->getEmail()."</td>";
				
				
				$text .= "</tr>";
			}
			if($_SESSION['cont'] >= 5) {
				$_SESSION['cont'] = 0;
			}		
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarTipoRepuesto.php?respuesta=error");
		}
	}
	
	}



 ?>