<?php

require_once('../Modelo/Pedido.php');
if (!session_id())session_start();

if(!empty($_GET['action'])){
	ctrPedido::main($_GET['action']);
	
}

	class ctrPedido{
	
	static function main($action){
		$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrPedido::registrar();
		}else if ($action == "buscarID"){
			ctrPedido::buscarID($_POST['idPedido']);
		}else if ($action == "actualizar"){
			ctrPedido::actualizar();
		}
		
	}
	static public function buscarID ($id){
		try {
			return Pedido::buscarForId($id);
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function buscarAll (){
		try {
			return Pedido::buscarAll();
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function buscar ($campo, $parametro){
		try {
			return Pedido::buscar($campo, $parametro);
		} catch (Exception $e) {
			header("");
		}
	}

    static public function inactivar($id){
	
	}
	
	static public function registrar (){
		try {
			$arrayPedido = array();
			$arrayPedido['fecha'] = date("Y-m-d");
			//$arrayPedido['fecha'] = implode ('-',array_reverse(explode('/',$arrayPedido['fecha'])));
			$arrayPedido['referencia'] = $_POST['referencia'];
			
			$arrayPedido['proveedor'] = Proveedor::buscarForId($_POST['idProveedor']);
			$arrayPedido['persona'] = Persona::buscarForId($_POST['idPersona']);
			
			$Pedido = new Pedido ($arrayPedido);
			$Pedido->registrar();
			header("Location: ../Vista/BuscarPedido.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/RegistrarPedido.php?respuesta=error");
		}
	}
	static public function actualizar (){
		try {
			$arrayPedido = array();
			$arrayPedido['fecha'] = $_POST['fecha'];
			$arrayPedido['fecha'] = implode ('-',array_reverse(explode('/',$arrayPedido['fecha'])));
			$arrayPedido['referencia'] = $_POST['referencia'];
			$arrayPedido['estado'] = $_POST['estado'];
			$arrayPedido['proveedor'] = Proveedor::buscarForId($_POST['idProveedor']);
			$arrayPedido['persona'] = Persona::buscarForId($_POST['idPersona']);
			$arrayPedido['idPedido'] = $_POST['idPedido'];
			
			$Pedido = new Pedido ($arrayPedido);
			$Pedido->actualizar();
			header("Location: ../Vista/BuscarPedido.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarPedido.php?respuesta=correcto");
		}
	}
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrapedido(this.value)";
			$arrPedido = ctrPedido::buscarAll();
			$text .= "<option selected value='0'>Seleccione una opción</option>";
			foreach ($arrPedido as $Pedido){
				$text .= "<option value=".$Pedido->getIdPedido().">".$Pedido->getReferencia()."</option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	
	static public function rowsPedido (){
		try {
			$arrPedido = ctrPedido::buscarAll ();
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Fecha de Pedido </th>";
			           
			            $text .= " <th> Referencia</th>";
			            $text .= " <th class='hidden-xs'>Proveedor</th>";
			            $text .= " <th>Vendedor</th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrPedido as $pedido){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$pedido->getIdPedido()."</td>";
					$text .= "      <td class='hidden-xs'>".$pedido->getFecha()."</td>";
					$text .= "      <td >".$pedido->getReferencia()."</td>";
					$text .= "      <td> ".$pedido->getProveedor()->getRazonSocial()."</td>";
					$text .= "      <td> ".$pedido->getPersona()->getNombres()."</td>";
					
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							   
							   
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Share";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	

	static public function getUpdate ($name,$id){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$Repuesto = ctrPedido::buscarID($id);
			
			$text .= "<option value=".$Repuesto->getIdPedido().">".$Repuesto->getIdPedido()."</option>";
			
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function TexboxPedido($id){
		try {
			$arrPedido = array();
			$arrPedido = ctrPedido::buscarID($id);
			
			$text ="<div class='form-group'>";
			$text .="<label class='control-label'> Fecha de Pedida <span class='symbol required'></span></label>";
			$text .= "<input type='date' class='form-control' id='fecha' name='fecha' value='".$arrPedido->getFecha()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";
			$text .="<label class='control-label'>Referencia <span class='symbol required'></span></label>";
			$text .="<input type='text'  class='form-control' id='referencia' name='referencia' value='".$arrPedido->getReferencia()."'>";
			$text .="</div>";
			 
		
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Proveedor<span class='symbol required'></span> </label>";
			$text .="<input type='text'  class='form-control' id='idProveedor' name='idProveedor' value='".$arrPedido->getProveedor()->getIdProveedor()."١ ".$arrPedido->getProveedor()->getRazonSocial()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'>Vendedor <span class='symbol required'></span> </label>";
			$text .="<input type='text'  class='form-control' id='idPersona' name='idPersona' value='".$arrPedido->getPersona()->getIdPersona()."١ ".$arrPedido->getPersona()->getNombres()."'>";
			$text .="</div >";
			
			
			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarPedido.php?respuesta=error");
		}
	}
	
	
	static public function rowwPedido($name){
		try {
			
			$arrPedido = ctrPedido::buscar('referencia',$name);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th class='center'>Fecha</th>";
			            $text .= " <th class='hidden-xs'>Referencia</th>";
						   $text .= " <th class='hidden-xs'>Proveedor</th>";
			            $text .= " <th class='hidden-xs'>Empleado</th>";
						
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <tbody>";
			foreach ($arrPedido as $pedido){
			
					if ($_SESSION['cont'] == 1){
						$text .= "<tr class='success'>";
					}else if ($_SESSION['cont'] == 2){
						$text .= "<tr class='active'>";
					}else if ($_SESSION['cont'] == 3){
						$text .= "<tr class='info'>";
					}else if ($_SESSION['cont'] == 4){
						$text .= "<tr class='warning'>";
					}else if ($_SESSION['cont'] == 5){
						$text .= "<tr class='danger'>";
					}
					$text .= "      <td class='center'>".$pedido->getIdPedido()."</td>";
					$text .= "      <td class='center'>".$pedido->getFecha()."</td>";
					$text .= "      <td >".$pedido->getReferencia()."</td>";
					$text .= "      <td class='hidden-xs'>".$pedido->getProveedor()->getRazonSocial()."</td>";
					$text .= "      <td class='hidden-xs'>".$pedido->getPersona()->getNombres()."</td>";
					
				
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							   
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								$text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
			}
			if($_SESSION['cont'] >= 5) {
				$_SESSION['cont'] = 0;
			}		
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/ConsultarPedido.php?respuesta=error");
		}
	}


	
}
	

?>