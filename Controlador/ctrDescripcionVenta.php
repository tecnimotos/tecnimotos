<?php

require_once('../Modelo/DescripcionVenta.php');
if (!session_id())session_start();


if(!empty($_REQUEST['action'])){
	
	ctrDescripcionVenta::main($_REQUEST['action']);
}

	

	class ctrDescripcionVenta{
	
	static function main($action){
		//$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrDescripcionVenta::registrar();
		}else if ($action == "buscarID"){
			ctrDescripcionVenta::buscarID($_POST['idDescripcionVenta']);
		}
		
	}
	 static public function buscarID ($id){
		try {
			return DescripcionVenta::buscarForId($id);
		} catch (Exception $e) {
			//header("");
		}
		
	}
	static public function buscarAll (){
		try {
			return DescripcionVenta::buscarAll();
		} catch (Exception $e) {
			header("");
		}
	}
	static public function buscar ($campo, $parametro){
		try {
			return DescripcionVenta::buscar($campo, $parametro);
		} catch (Exception $e) {
			return false;		}
	}
	
	static public function inactivar($id){
	
	}
	
	static public function registrar (){
		try {
			$arrayDescripcionVenta = array();
			$arrayDescripcionVenta['cantidad'] = $_POST['cantidad'];
			$arrayDescripcionVenta['valorUnitario'] = $_POST['valorUnitario'];
			$arrayDescripcionVenta['descripcion'] = $_POST['descripcion'];
			$arrayDescripcionVenta['valorProducto'] = $_POST['valorProducto'];
			$arrayDescripcionVenta['venta'] = Venta::buscarForId($_POST['idVenta']);;
			$Rep = Repuesto::buscarForId($_POST['idRepuesto']);
			//$Rep->setEstado('Inactivo');
			//$Rep->actualizar();
			$arrayDescripcionVenta['repuesto'] = $Rep;
			
			
			$DescripcionVenta = new DescripcionVenta ($arrayDescripcionVenta);
			if ( $DescripcionVenta->registrar()){
				$arrayDescripcionVenta['repuesto'] -> setStockActual(
							$arrayDescripcionVenta['repuesto'] -> getStockActual() - 
							$arrayDescripcionVenta ['cantidad']
							);
				$arrayDescripcionVenta['repuesto'] -> actualizar();
				}
				$arrDescripcionVenta = ctrDescripcionVenta::buscar('venta',$_SESSION['idVenta']);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th class='center'> Cantidad </th>";
			            $text .= " <th class='hidden-xs'>Valor Unitario</th>";
			            $text .= " <th> Descripcion</th>";
			            $text .= " <th class='hidden-xs'>Valor de Producto</th>";
			            $text .= " <th >Venta</th>";
						$text .= " <th >Repuesto</th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrDescripcionVenta as $desventa){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$desventa->getIdDescripcionVenta()."</td>";
					$text .= "      <td class='center'>".$desventa->getCantidad()."</td>";
					$text .= "      <td > ".$desventa->getValorUnitario()."</td>";
					$text .= "      <td > ".$desventa->getDescripcion()."</td>";
					$text .= "      <td > ".$desventa->getValorProducto()."</td>";
					$text .= "      <td > ".$desventa->getVenta()->getReferencia()."</td>";
					$text .= "      <td > ".$desventa->getRepuesto()->getReferencia()."</td>";
					
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    
							    
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			echo $text;
			
			//header("Location: ../Vista/BuscarDescripcionVenta.php?respuesta=correcto");
		} catch (Exception $e) {
			//header("Location: ../Vista/BuscarDescripcionVenta.php?respuesta=error");
		}
	}
	
	
	
	
	
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$arrDescripcionVenta = ctrDescripcionVenta::buscarAll();
			$text .= "<option selected value='0'>Seleccione una opción</option>";
			foreach ($arrDescripcionVenta as $DescripcionVenta){
				$text .= "<option value=".$DescripcionVenta->getIdDescripcionVenta().">".$DescripcionVenta->getIdDescripcionVenta()." ".$DescripcionVenta->getCantidad()."</option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function rowsDescripcionVenta (){
		try {
			$arrDescripcionVenta = ctrDescripcionVenta::buscarAll ();
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th class='center'> Cantidad </th>";
			            $text .= " <th class='hidden-xs'>Valor Unitario</th>";
			            $text .= " <th> Descripcion</th>";
			            $text .= " <th class='hidden-xs'>Valor de Producto</th>";
			            $text .= " <th >Venta</th>";
						$text .= " <th >Repuesto</th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrDescripcionVenta as $desventa){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$desventa->getIdDescripcionVenta()."</td>";
					$text .= "      <td class='center'>".$desventa->getCantidad()."</td>";
					$text .= "      <td > ".$desventa->getValorUnitario()."</td>";
					$text .= "      <td > ".$desventa->getDescripcion()."</td>";
					$text .= "      <td > ".$desventa->getValorProducto()."</td>";
					$text .= "      <td > ".$desventa->getVenta()->getReferencia()."</td>";
					$text .= "      <td > ".$desventa->getRepuesto()->getReferencia()."</td>";
					
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    
							    
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	
	
	
	static public function rowConsultar($valorVenta){
		try {
			
			$arrDescripcionVenta = ctrDescripcionVenta::buscar('valorUnitario',$valorVenta);
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Cantidad </th>";
			            $text .= " <th class='hidden-xs'>Valor Unitario</th>";
						$text .= " <th class='hidden-xs'>Descripcion</th>";
						$text .= " <th class='hidden-xs'>Valor de Productos</th>";
			            $text .= " <th class='hidden-xs'>Venta</th>";
						$text .= " <th class='hidden-xs'>Repuesto</th>";
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <tbody>";
			foreach ($arrDescripcionVenta as $DescripcionVenta){
			
					if ($_SESSION['cont'] == 1){
						$text .= "<tr class='success'>";
					}else if ($_SESSION['cont'] == 2){
						$text .= "<tr class='active'>";
					}else if ($_SESSION['cont'] == 3){
						$text .= "<tr class='info'>";
					}else if ($_SESSION['cont'] == 4){
						$text .= "<tr class='warning'>";
					}else if ($_SESSION['cont'] == 5){
						$text .= "<tr class='danger'>";
					}
					$text .= "      <td class='center'>".$DescripcionVenta->getIdDescripcionVenta()."</td>";
					$text .= "      <td class='hidden-xs'>".$DescripcionVenta->getCantidad()."</td>";
					$text .= "      <td >".$DescripcionVenta->getValorUnitario()."</td>";
					$text .= "      <td >".$DescripcionVenta->getDescripcion()."</td>";
					$text .= "      <td >".$DescripcionVenta->getValorProducto()."</td>";
					$text .= "      <td > ".$DescripcionVenta->getVenta()->getReferencia()."</td>";
					$text .= "      <td> ".$DescripcionVenta->getRepuesto()->getReferencia()."</td>";
					$text .= "		<td class='center'> ";

						$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							    $text .= " <a href= 'ModificarDescripcionVenta.php?idDesVenta=".$DescripcionVenta->getIdDescripcionVenta()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Edit'><i class='fa fa-edit'></i></a>";
							    
					    $text .= "</div>";

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
			}
			if($_SESSION['cont'] >= 5) {
				$_SESSION['cont'] = 0;
			}		
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarDescripcionVenta.php?respuesta=error");
		}
	}
	
	}
?>