<?php

require_once('../Modelo/Novedad.php');
session_start();
if(!empty($_GET['action'])){
	ctrNovedad::main($_GET['action']);
	
}

	class ctrNovedad{
	
	static function main($action){
		$_SESSION['cont'] = $_SESSION['cont'] + 1;
		if ($action == "registrar"){
			ctrNovedad::registrar();
		}else if ($action == "buscarID"){
			ctrNovedad::buscarID($_POST['idNovedad']);
		}else if ($action == "actualizar"){
			ctrNovedad::actualizar();
		}
		
	}
	static public function buscarID ($id){
		try {
			return Novedad::buscarForId($id);
		} catch (Exception $e) {
			//header("");
		}
	}
	
	static public function buscarAll (){
		try {
			return Novedad::buscarAll();
		} catch (Exception $e) {
			header("");
		}
	}
	
	static public function buscar ($campo, $parametro){
		try {
			return Novedad::buscar($campo, $parametro);
		} catch (Exception $e) {
			header("");
		}
	}
	static public function buscarCambio(){
		try {
			return Novedad::buscarCambio();
		} catch (Exception $e) {
			header("");
		}
		
		}

    static public function inactivar($id){
	
	}
	
	static public function registrar (){
		try {
			$arrayNovedad = array();
			$arrayNovedad['tipoNovedad'] = $_POST['tipoNovedad'];
			$arrayNovedad['fechaNovedad'] = $_POST['fechaNovedad'];
			$arrayNovedad['fechaNovedad'] = implode ('-',array_reverse(explode('/',$arrayNovedad['fechaNovedad'])));
			$arrayNovedad['descripcion'] = $_POST['descripcion'];
			$arrayNovedad['excedente'] = $_POST['excedente'];
			$arrayNovedad['reembolso'] = $_POST['reembolso'];
			$arrayNovedad['repuestoEntrante'] = Repuesto::buscarForId($_POST['idRepuesto']);
			$arrayNovedad['repuestoSaliente'] = Repuesto::buscarForId($_POST['idRepuesto']);
			$arrayNovedad['venta'] = Venta::buscarForId($_POST['idVenta']);
			
			$novedad = new Novedad ($arrayNovedad);
		
			if ( $novedad->registrar()){
				$novedad['repuestoEntrante'] -> setStockActual(
							$arrayNovedad['repuestoEntrante'] -> getStockActual() + 
							$arrayNovedad ['cantidad']
							
							
							);
							$novedad['repuestoSaliente'] -> setStockActual(
							$arrayNovedad['repuestoSaliente'] -> getStockActual() + 
							$arrayNovedad ['cantidad']);
							
				$arrayNovedad['repuestoSaliente'] -> actualizar();
				$arrayNovedad['repuestoEntrante'] -> actualizar();
				
				}
			header("Location: ../Vista/BuscarNovedad.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/RegistrarNovedad.php?respuesta=error");
		}
	}
	static public function actualizar (){
	try {
		
			$arrayNovedad = array();
			$arrayNovedad['tipoNovedad'] = $_POST['tipoNovedad'];
			$arrayNovedad['fechaNovedad'] = $_POST['fechaNovedad'];
			$arrayNovedad['fechaNovedad'] = implode ('-',array_reverse(explode('/',$arrayNovedad['fechaNovedad'])));
			$arrayNovedad['descripcion'] = $_POST['descripcion'];
			$arrayNovedad['excedente'] = $_POST['excedente'];
			$arrayNovedad['reembolso'] = $_POST['reembolso'];
			$arrayNovedad['repuestoEntrante'] = Repuesto::buscarForId($_POST['idRepuesto']);
			$arrayNovedad['repuestoSaliente'] = Repuesto::buscarForId($_POST['idRepuesto']);
			$arrayNovedad['venta'] = Venta::buscarForId($_POST['idVenta']);
			
		
			$arrayNovedad['idNovedad'] = $_POST['idNovedad'];
			
			$Novedad = new Novedad ($arrayNovedad);
			$Novedad->actualizar();
			header("Location: ../Vista/BuscarNovedad.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../Vista/ModificarNovedad.php?respuesta=correcto");
		}
	}
	
	static public function getList ($name){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$arrVenta = ctrNovedad::buscarAll();
			$text .= "<option selected value='0'>Seleccione una opción</option>";
			foreach ($arrVenta as $Pedido){
				$text .= "<option value=".$novedad->getIdNovedad().">".$novedad->getIdVenta()->getReferencia()."</option>";
			}
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
static public function rowsNovedad (){
	
	
		try {
			$arrnovedad = ctrNovedad::buscarAll();
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Tipo de Novedad </th>";
			            $text .= " <th class='hidden-xs'>Fecha</th>";
			            $text .= " <th> Descripcion</th>";
			            $text .= " <th class='hidden-xs'>Excedente</th>";
			            $text .= " <th>Reembolso</th>";
						$text .= " <th>Repuesto Entrante</th>";
						$text .= " <th>Repuesto Saliente</th>";
						$text .= " <th>Venta</th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			
			$text .= "  <tbody>";
			
			$cont = 0;
			
				foreach ($arrnovedad as $novedad){
					$cont++;
					if ($cont == 1){
						$text .= "<tr class='success'>";
					}else if ($cont == 2){
						$text .= "<tr class='active'>";
					}else if ($cont == 3){
						$text .= "<tr class='info'>";
					}else if ($cont == 4){
						$text .= "<tr class='warning'>";
					}else if ($cont == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$novedad->getIdNovedad()."</td>";
					$text .= "      <td class='hidden-xs'>".$novedad->getTipoNovedad()."</td>";
					$text .= "      <td class='hidden-xs'>".$novedad->getFechaNovedad()."</td>";
					$text .= "      <td >".$novedad->getDescripcion()."</td>";
					$text .= "      <td >".$novedad->getReembolso()."</td>";
					$text .= "      <td >".$novedad->getExcedente()."</td>";
					$text .= "      <td >".$novedad->getRepuestoEntrante()->getReferencia()."</td>";
					$text .= "      <td >".$novedad->getRepuestoSaliente()->getReferencia()."</td>";
					$text .= "      <td >".$novedad->getVenta()->getFechaVenta()."</td>";
					
					
					$text .= "		<td class='center'> ";

						//$text .= "<div class='visible-md visible-lg hidden-sm hidden-xs'>";
							//    $text .= " <a href= '../Vista/ModificarNovedad.php?idNov=".$novedad->getIdNovedad()."' class= 'btn btn-xs btn-blue tooltips' data-placement= 'top' data-original-title='Editar'><i class='fa fa-edit'></i></a>";
							   
							   

					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Share";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
					if($cont == 5) {
						$cont = 0;
					}
				}
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
			
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	static public function TexboxNovedad($id){
		try {
			$arrNovedad = array();
			$arrNovedad = ctrNovedad::buscarID($id);
			
			$text ="<div class='form-group'>";
			$text .="<label class='control-label'>Tipo de Novedad<span class='symbol required'></span></label>";
			$text .= "<input type='text'  class='form-control' id='tipoNovedad' name='tipoNovedad' value='".$arrNovedad->getTipoNovedad()."'>";
			$text .="</div >";
			 
			$text .="<div class='form-group'>";
			$text .="<label class='control-label'>Fecha de Novedad <span class='symbol required'></span></label>";
			$text .="<input type='text'  class='form-control' id='fechaNovedad' name='fechaNovedad' value='".$arrNovedad->getFechaNovedad()."'>";
			$text .="</div>";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'> Descripcion <span class='symbol required'></span> </label>";
			$text .="<input type=type='text'  class='form-control' id='descripcion' name='descripcion' value='".$arrNovedad->getDescripcion()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'> Excedente <span class='symbol required'></span> </label>";
			$text .="<input type=type='text'  class='form-control' id='excedente' name='excedente' value='".$arrNovedad->getExcedente()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'> Reembolso <span class='symbol required'></span> </label>";
			$text .="<input type=type='text'  class='form-control' id='reembolso' name='reembolso' value='".$arrNovedad->getReembolso()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'> Repuesto Entrante <span class='symbol required'></span> </label>";
			$text .="<input type=type='text'  class='form-control' id='repuestoEntrante' name='repuestoEntrante' value='".$arrNovedad->getRepuestoEntrante()->getReferencia()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'> Repuesto Saliente <span class='symbol required'></span> </label>";
			$text .="<input type=type='text'  class='form-control' id='repuestoSaliente' name='repuestoSaliente' value='".$arrNovedad->getRepuestoSaliente()->getReferencia()."'>";
			$text .="</div >";
			
			$text .="<div class='form-group'>";		
			$text .="<label class='control-label'> Venta <span class='symbol required'></span> </label>";
			$text .="<input type=type='text'  class='form-control' id='venta' name='venta' value='".$arrNovedad->getVenta()->getFechaVenta()."'>";
			$text .="</div >";

			return $text;
			
		} catch (Exception $e) {
			header("Location: ../Vista/BuscarNovedad.php?respuesta=error");
		}

	}
	
	static public function getUpdate ($name,$id){
		try {
			$text = "<select name='".$name."' id='".$name."'>". "onchange = muestrarepuesto(this.value)";
			$text .= "<option value=".$id.">".$id."</option>";
			$text .= "</select>";			
			return $text;
		} catch (Exception $e) {
			return "Error al cargar los datos";
		}
	}
	
	
		
	static public function rowConsultar($name){
		try {
			
			$arrNovedad = ctrNovedad::buscar('fechaNovedad',$name);
			//echo $_SESSION['cont'];
			$text = "<table class= 'table table-hover' id= 'sample-table-1'>";
			
			$text .= "<thead>";
			    $text .= " <tr>";
			            $text .= " <th class='center'>#</th>";
			            $text .= " <th> Tipo de Novedad</th>";
			            $text .= " <th>Fecha de Novedad</th>";
			            $text .= " <th >Descripcion</th>";
						$text .= " <th >Excedente</th>";
						$text .= " <th >Reembolso</th>";
						$text .= " <th >Repuesto Entrante</th>";
						$text .= " <th >Repuesto Saliente</th>";
			            $text .= " <th></th>";
			    $text .= " </tr>";
	    	$text .= "</thead>";
			$text .= "  <tbody>";
			foreach ($arrNovedad as $novedad){
				
					if ($_SESSION['cont'] == 1){
						$text .= "<tr class='success'>";
					}else if ($_SESSION['cont'] == 2){
						$text .= "<tr class='active'>";
					}else if ($_SESSION['cont'] == 3){
						$text .= "<tr class='info'>";
					}else if ($_SESSION['cont'] == 4){
						$text .= "<tr class='warning'>";
					}else if ($_SESSION['cont'] == 5){
						$text .= "<tr class='danger'>";
					}
					
					$text .= "      <td class='center'>".$novedad->getIdNovedad()."</td>";
					$text .= "      <td >".$novedad->getTipoNovedad()."</td>";
					$text .= "      <td >".$novedad->getFechaNovedad()."</td>";
					$text .= "      <td> ".$novedad->getDescripcion()."</td>";
					$text .= "      <td> ".$novedad->getExcedente()."</td>";
					$text .= "      <td> ".$novedad->getReembolso()."</td>";
					$text .= "      <td> ".$novedad->getRepuestoEntrante()->getReferencia()."</td>";
					$text .= "      <td> ".$novedad->getRepuestoSaliente()->getReferencia()."</td>";
					$text .= "      <td> ".$novedad->getVenta()->getFechaVenta()."</td>";
					$text .= "		<td class='center'> ";

						
					    $text .= "</div>";
						
					    $text .= "<div class= 'visible-xs visible-sm hidden-md hidden-lg'>";
						    $text .= "<div class= 'btn-group'>";
							    $text .= "<a class= 'btn btn-green dropdown-toggle btn-sm' data-toggle= 'dropdown' href= '#'>";
							    	$text .= "<i class= 'fa fa-cog'></i> <span class= 'caret'></span>";
							    $text .= "</a>";
							    $text .= "<ul role='menu' class='dropdown-menu pull-right dropdown-dark'>";
							    	$text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-edit'></i> Edit";
									    $text .= "</a>";

								    $text .= "</li>";

								    $text .= "<li>";

									    $text .= "<a role= 'menuitem' tabindex= '-1' href= '#'>";
									    	$text .= "<i class= 'fa fa-share'></i> Share";
									    $text .= "</a>";

								    $text .= "</li>";

									$text .= "<li>";

								    	$text .= "<a role= 'menuitem' tabindex= '-1' href='#'>";
										    $text .= "<i class='fa fa-times'></i> Inactivar";
										$text .= "</a>";

								    $text .= "</li>";
									
							    $text .= "</ul>";
						    $text .= "</div>";
					    $text .= "</div>";
					$text .= "		</td>";
					$text .= "</tr>";
			}
			if($_SESSION['cont'] >= 5) {
				$_SESSION['cont'] = 0;
			}		
			$text .= "  </tbody>";
			$text .= "</table>";	
			return $text;
		} catch (Exception $e) {
			header("Location: ../Vista/InactivarTipoRepuesto.php?respuesta=error");
		}
	}
}

?>