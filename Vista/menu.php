<?php
require_once('../Modelo/Empleado.php');
require_once('../Modelo/Persona.php');
require_once('../Controlador/ctrEmpleado.php');
require_once ('../Controlador/ctrPersona.php');
 
?>
                	<!-- start: SIDEBAR -->
				  <div class="main-navigation left-wrapper transition-left">
						<div class="navigation-toggler hidden-sm hidden-xs">
							<a href="#main-navbar" class="sb-toggle-left">
							</a>
						</div>
					<div class="user-profile border-top padding-horizontal-10 block">
							<div class="inline-block">
								<img src="assets/images/moto1.jpg" alt="">
							</div>
                            
							<div class="inline-block">
								<h5 class="no-margin"> Bienvenido   <?php echo $_SESSION['usuario']->getNombres(); ?></h5>
								<h4 class="no-margin">TecniMotos</h4>
                                <h5 class="no-margin"><?php echo $_SESSION['usuario']->getTipoUsuario(); ?></h5>
								<a class="btn user-options sb_toggle">
									<i class="fa fa-cog"></i>
								</a>
							</div>
					  </div>
					  <!-- start: MAIN NAVIGATION MENU -->
					  <ul class="main-navigation-menu">
						  <li class="active open">
						     <li>
							  <a href="javascript:void(0)"><i class="fa fa-user"></i> <span class="title">Login</span><i class="icon-arrow"></i> </a>
							  <ul class="sub-menu">
									<li>
									  <a href="RegistrarPersona.php">
										 <i class="fa fa-pencil-square-o"></i> <span class="title"> Formulario de Registro  </span>
									  </a>
								  </li>
								 
								 
							  </ul>
						  </li>
                          <?php if ($_SESSION['usuario']->getTipoUsuario() == "Administrador"){ ?>
						  <li>
							  <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Novedad </span><i class="icon-arrow"></i> </a>
							  <ul class="sub-menu">
								  <li>
									  <a href="RegistrarCambio.php">
										 <i class="fa fa-pencil-square-o"></i> <span class="title">Registrar Cambio</span>
									  </a>
								  </li>
								  <li>
									  <a href="BuscarCambio.php">
										 <i class="fa fa-folder-open"></i> <span class="title">Buscar Cambios</span>
									  </a>
								  </li>
<li>
									  <a href="ConsultarCambios.php">
										   <i class="fa fa-folder"></i><span class="title">Consultar Cambio</span>
									  </a>
								  </li>

							  </ul>
						  </li>
					     <?php } ?>
                         
                          <?php if ($_SESSION['usuario']->getTipoUsuario() == "Administrador"){ ?>
						  <li>
							  <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Proveedor</span><i class="icon-arrow"></i> </a>
							  <ul class="sub-menu">
								  <li>
									  <a href="RegistrarProveedor.php">
										 <i class="fa fa-pencil-square-o"></i> <span class="title">Registrar Proveedor</span>
									  </a>
								  </li>
								  <li>
									  <a href="BuscarProveedor.php">
										 <i class="fa fa-folder-open"></i>  <span class="title">Buscar Proveedores</span>
									  </a>
								  </li>
<li>
									  <a href="ConsultarProveedor.php">
										 <i class="fa fa-folder"></i> <span class="title">Consultar Proveedor</span>
									  </a>
								  </li>

							  </ul>
						  </li>
					     <?php } ?>
                         
                         
                       <?php if ($_SESSION['usuario']->getTipoUsuario() == "Administrador" || $_SESSION['usuario']->getTipoUsuario() == "Vendedor"){ ?>
						  <li>
							  <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Venta</span><i class="icon-arrow"></i> </a>
							  <ul class="sub-menu">
								  <li>
									  <a href="RegistrarVenta.php">
										  <i class="fa fa-pencil-square-o"></i> <span class="title">Registrar Venta</span>
									  </a>
								  </li>
                                   <li>
									  <a href="RegistrarDescripcionVenta.php">
										 <i class="fa fa-pencil-square-o"></i> <span class="title">Registrar Descripcion Venta</span>
									  </a>
								  </li>
								  <li>
									  <a href="BuscarVenta.php">
										  <i class="fa fa-folder-open"></i> <span class="title">Buscar Venta</span>
									  </a>
								  </li>
                                  
                                  <li>
									  <a href="BuscarDescripcionVenta.php">
										 <i class="fa fa-folder-open"></i> <span class="title">Buscar Descripcion Venta</span>
									  </a>
								  </li>
<li>
									  <a href="ConsultarVenta.php">
										<i class="fa fa-folder"></i>  <span class="title">Consultar Venta</span>
									  </a>
                                      
                                      
								 
								  
<li>
									  <a href="ConsultarDescripcionVenta.php">
										<i class="fa fa-folder"></i>  <span class="title">Consultar Descripcion Venta</span>
									  </a>
								  </li>
							  </ul>
						  </li>
					     <?php } ?>
                         
                         
                          <?php if ($_SESSION['usuario']->getTipoUsuario() == "Administrador"){ ?>
						  <li>
							  <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Pedido</span><i class="icon-arrow"></i> </a>
							  <ul class="sub-menu">
								  <li>
									  <a href="RegistrarPedido.php">
										  <i class="fa fa-pencil-square-o"></i><span class="title">Registrar Pedido</span>
									  </a>
								  </li>
                                  <li>
									  <a href="RegistrarDescripcionPedido.php">
										  <i class="fa fa-pencil-square-o"></i><span class="title">Registrar Descripcion Pedido</span>
									  </a>
								  </li>
								  <li>
									  <a href="BuscarPedido.php">
										  <i class="fa fa-folder-open"></i> <span class="title">Buscar Pedido</span>
									  </a>
								  </li>
                                  <li>
									  <a href="BuscarDescripcionPedido.php">
										  <i class="fa fa-folder-open"></i> <span class="title">Buscar Descripcion Pedido</span>
									  </a>
								  </li>
<li>
									  <a href="ConsultarPedido.php">
										<i class="fa fa-folder"></i>  <span class="title">Consultar Pedido</span>
									  </a>
                                      
                                      
								  
<li>
									  <a href="ConsultarDescripcionPedido.php">
										<i class="fa fa-folder"></i>   <span class="title">Consultar Descripcion Pedido</span>
									  </a>
								  </li>
							  </ul>
						  </li>
					     <?php } ?>
                         
                        
                         
                         <?php if ($_SESSION['usuario']->getTipoUsuario() == "Administrador"){ ?>
						  <li>
							  <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Unidad Medida</span><i class="icon-arrow"></i> </a>
							  <ul class="sub-menu">
								  <li>
									  <a href="RegistrarUnidadMedida.php">
										 <i class="fa fa-pencil-square-o"></i> <span class="title">Registrar  Unidad Medida</span>
									  </a>
								  </li>
								  <li>
									  <a href="BuscarUnidadMedida.php">
										 <i class="fa fa-folder-open"></i>  <span class="title">Buscar  Unidad Medida</span>
									  </a>
								  </li>
<li>
									  <a href="ConsultarUnidadMedida.php">
										<i class="fa fa-folder"></i>    <span class="title">Consultar  Unidad Medida</span>
									  </a>
								  </li>

							  </ul>
						  </li>
					     <?php } ?>
                          <?php if ($_SESSION['usuario']->getTipoUsuario() == "Administrador"){ ?>
						  <li>
							  <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Tipo Repuesto</span><i class="icon-arrow"></i> </a>
							  <ul class="sub-menu">
								  <li>
									  <a href="RegistrarTipoRepuesto.php">
										 <i class="fa fa-pencil-square-o"></i> <span class="title">Registrar Tipo Repuesto</span>
									  </a>
								  </li>
								  <li>
									  <a href="BuscarTipoRepuesto.php">
										<i class="fa fa-folder-open"></i>  <span class="title">Buscar Tipo Repuesto</span>
									  </a>
								  </li>
<li>
									  <a href="ConsultarTipoRepuesto.php">
										  <i class="fa fa-folder"></i> <span class="title">Consultar Tipo Repuesto</span>
									  </a>
								  </li>

							  </ul>
						  </li>
					     <?php } ?>
                         
                          <?php if ($_SESSION['usuario']->getTipoUsuario() == "Administrador"){ ?>
						  <li>
							  <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Repuesto</span><i class="icon-arrow"></i> </a>
							  <ul class="sub-menu">
								  <li>
									  <a href="RegistrarRepuesto.php">
										 <i class="fa fa-pencil-square-o"></i>  <span class="title">Registrar Repuesto</span>
									  </a>
								  </li>
								  <li>
									  <a href="BuscarRepuesto.php">
										 <i class="fa fa-folder-open"></i> <span class="title">Buscar Repuesto</span>
									  </a>
								  </li>
<li>
									  <a href="ConsultarRepuesto.php">
										<i class="fa fa-folder"></i>  <span class="title">Consultar Repuesto</span>
									  </a>
								  </li>

							  </ul>
						  </li>
					     <?php } ?>
                          <?php if ($_SESSION['usuario']->getTipoUsuario() == "Administrador"){ ?>
						  <li>
							  <a href="javascript:void(0)"><i class="fa fa-folder"></i> <span class="title"> Consultar Usuarios</span><i class="icon-arrow"></i> </a>
							  <ul class="sub-menu">
								  
								  <li>
									  <a href="ConsultarCliente.php">
										  <span class="title">consultar Clientes</span>
									  </a>
								  </li>
<li>
									  <a href="ConsultarVendedor.php">
										<span class="title">Consultar Vendedor</span>
									  </a>
								  </li>

							  </ul>
						  </li>
					     <?php } ?>
                         <?php if ($_SESSION['usuario']->getTipoUsuario() == "Administrador"){ ?>
						  <li>
							  <a href="javascript:void(0)"><i class="fa fa-folder-open"></i> <span class="title"> Busquedas de  Usuarios</span><i class="icon-arrow"></i> </a>
							  <ul class="sub-menu">
								  
								  <li>
									  <a href="BuscarCliente.php">
										  <span class="title">Buscar Clientes</span>
									  </a>
								  </li>
<li>
									  <a href="BuscarVendedor.php">
										  <span class="title">Buscar Vendedor</span>
									  </a>
								  </li>

							  </ul>
						  </li>
					     <?php } ?>
                         
                         
                           
                          
                          <li>
							 
								  
							  </ul>
						  </li>
						  </li>
					  </ul>
					  <!-- end: MAIN NAVIGATION MENU -->
					</div>
					<!-- end: SIDEBAR -->